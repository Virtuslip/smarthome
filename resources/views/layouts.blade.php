<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Material Design for Bootstrap</title>

    <link rel="icon" href="{{asset('assets/vendors/MDB/img/mdb-favicon.ico')}}" type="image/x-icon">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
    <link rel="stylesheet" href="{{asset('assets/vendors/MDB/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendors/MDB/css/mdb.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendors/MDB/css/style.css')}}">
</head>
<body>
<header>
    <nav class="navbar navbar-expand-lg navbar-dark pink scrolling-navbar">
        <a class="navbar-brand" href="#"><strong>SmartHome</strong></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Features</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Pricing</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Opinions</a>
                </li>
            </ul>
            <ul class="navbar-nav nav-flex-icons">
                <li class="nav-item">
                    <a class="nav-link"><i class="fab fa-facebook-f"></i></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link"><i class="fab fa-twitter"></i></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link"><i class="fab fa-instagram"></i></a>
                </li>
            </ul>
        </div>
    </nav>

</header>
<div class="container pt-3">
    @yield('content')
</div>
<script type="text/javascript" src="{{asset('assets/vendors/MDB/js/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/MDB/js/popper.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/MDB/js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/MDB/js/mdb.min.js')}}"></script>
@yield('js-scripts')
</body>
</html>
