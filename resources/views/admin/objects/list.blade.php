@extends('layouts')

@section('content')
    <a href="{{route('admin.objects.create')}}" type="button" class="btn btn-primary">Создать</a>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">Символьный код</th>
            <th scope="col">Наименование</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($items as $item)
            <tr>
                <th>{{$item->code}}</th>
                <td><a href="{{route('admin.objects.edit', ['object' => $item->id])}}">{{$item->name}}</a></td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
