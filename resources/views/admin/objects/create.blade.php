@extends('layouts')

@section('content')
    <div class="card">
        <h5 class="card-header info-color white-text text-center py-4">
            <strong>Новый объект</strong>
        </h5>
        <div class="card-body px-lg-5 pt-0">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div><br/>
            @endif
            <form class="text-center" method="post" action="{{route('admin.objects.store')}}">
                @csrf
                <div class="md-form">
                    <input type="text" class="form-control" id="sCode" name="code">
                    <label for="sCode">Символьный код</label>
                </div>
                <div class="md-form">
                    <input type="text" class="form-control" id="sName" name="name">
                    <label for="sName">Наименование</label>
                </div>
                <button class="btn btn-info btn-block" type="submit">Создать</button>
            </form>
        </div>
    </div>
@endsection
