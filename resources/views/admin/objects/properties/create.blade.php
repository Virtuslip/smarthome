<form method="post" action="{{route('admin.object_props.store', ['sobject' => $object_id])}}">
    @csrf
    <div class="modal-header">
        <h5 class="modal-title">Новое свойство</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="modal-body">
        <div class="md-form">
            <input type="text" class="form-control" id="sPropCode" name="code">
            <label for="sPropCode">Символьный код</label>
        </div>
        <div class="md-form">
            <input type="text" class="form-control" id="sPropName" name="name">
            <label for="sPropName">Наименование</label>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
        <button type="submit" class="btn btn-success">Сохранить</button>
    </div>
</form>
