@extends('layouts')

@section('content')
    <ul class="nav nav-tabs" id="myTab" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" id="general-tab" data-toggle="tab" href="#general" role="tab"
               aria-controls="general"
               aria-selected="true">Общее</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="props-tab" data-toggle="tab" href="#props" role="tab" aria-controls="props"
               aria-selected="false">Свойства</a>
        </li>
    </ul>
    <div class="tab-content" id="tabContent">
        <div class="tab-pane fade py-3 show active" id="general" role="tabpanel" aria-labelledby="general-tab">
            <form method="post" action="{{route('admin.objects.update', ['object' => $item->id])}}">
                @csrf
                <div class="md-form">
                    <input type="text" class="form-control" id="sCode" name="code" value="{{$item->code}}">
                    <label for="sCode">Символьный код</label>
                </div>
                <div class="md-form">
                    <input type="text" class="form-control" id="sName" name="name" value="{{$item->name}}">
                    <label for="sName">Наименование</label>
                </div>
                <button class="btn btn-success" type="submit">Сохранить</button>
            </form>
        </div>
        <div class="tab-pane fade py-3" id="props" role="tabpanel" aria-labelledby="props-tab">
            <div class="text-right">
                <a href="{{route('admin.object_props.create', ['sobject' => $item->id])}}" id="addProperty" type="button" class="btn btn-primary">Добавить свойство</a>
            </div>
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">Символьный код</th>
                    <th scope="col">Наименование</th>
                    <th scope="col">Значение</th>
                </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>

    <div class="modal fade" id="modalProperty" tabindex="-1" role="dialog" aria-labelledby="modalProperty"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content" id="modalPropertyContent">

            </div>
        </div>
    </div>
@endsection

@section('js-scripts')
    <script type="text/javascript" src="{{asset('assets/js/admin/objects.js')}}"></script>
@endsection
