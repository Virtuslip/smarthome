require('./bootstrap');
import 'material-design-icons-iconfont/dist/material-design-icons.css'
window.Vue = require('vue');
import Vuetify from 'vuetify'

Vue.use(Vuetify);
import App from './App'
import VueRouter from 'vue-router'

Vue.use(VueRouter);

import routes from './routes';

const router = new VueRouter({
    mode: 'history',
    routes: routes
});

const app = new Vue({
    el: '#app',
    components: {App},
    vuetify: new Vuetify({
        icons: {
            iconfont: 'md',
        },
    }),
    router,

});
