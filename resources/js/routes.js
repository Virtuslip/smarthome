import ModuleList from "./pages/modules/ModuleList";
import SObjectList from "./pages/objects/SObjectList";
import SObjectEdit from "./pages/objects/SObjectEdit";
import ScriptsList from "./pages/scripts/ScriptsList";
import ScriptEdit from "./pages/scripts/ScriptEdit";
import ModuleEdit from "./pages/modules/ModuleEdit";
import DashboardList from "./pages/dashboards/DashboardList";
import DashboardEdit from "./pages/dashboards/DashboardEdit";
import PublicDasboard from "./public/dashboard/PublicDasboard";

const routes = [
    {
        path: '/',
        name: 'home',
        component: PublicDasboard
    },
    {
        path: '/modules/:id',
        name: 'module_detail',
        component: ModuleEdit
    },
    {
        path: '/modules',
        name: 'modules_list',
        component: ModuleList
    },
    {
        path: '/objects/:id',
        name: 'object_edit',
        component: SObjectEdit
    },
    {
        path: '/objects',
        name: 'objects_list',
        component: SObjectList
    },
    {
        path: '/scripts/:id',
        name: 'script_edit',
        component: ScriptEdit
    },
    {
        path: '/scripts',
        name: 'scripts_list',
        component: ScriptsList
    },
    {
        path: '/dashboards',
        name: 'dashboards_list',
        component: DashboardList
    },
    {
        path: '/dashboard/:id',
        name: 'dashboard_edit',
        component: DashboardEdit
    },
];
export default routes;
