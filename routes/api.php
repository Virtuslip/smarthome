<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/send-command', 'SObjectDataController@sendCommand');

Route::get('/module/edit/{id}', 'ModuleController@edit');
Route::get('/modules/list', 'ModuleController@index');
Route::post('/module/save', 'ModuleController@save');

Route::post('/object/save', 'SObjectController@save');
Route::get('/object/edit/{id}', 'SObjectController@edit');
Route::get('/object/{id}/remove', 'SObjectController@remove');

Route::get('/object/{id}/data/list', 'SObjectDataController@index');
Route::get('/object-data/{id}/remove', 'SObjectDataController@remove');
Route::post('/object/data/save', 'SObjectDataController@save');
Route::get('/object/{object}/data/{id}/edit', 'SObjectDataController@edit');
Route::get('/objects/list', 'SObjectController@index');

Route::post('/script/save', 'ScriptsController@save');
Route::get('/script/edit/{id}', 'ScriptsController@edit');
Route::get('/scripts/list', 'ScriptsController@index');
Route::get('/script/remove/{id}', 'ScriptsController@remove');

Route::get('/admin/dashboards/list', 'DashboardController@index');
Route::get('/admin/dashboard/remove', 'DashboardController@remove');
Route::get('/admin/dashboard/edit', 'DashboardController@edit');
Route::post('/admin/dashboards/save/{id}', 'DashboardController@save');

Route::get('/admin/dashboard-item/list', 'DashboardItemController@getList');
Route::get('/admin/dashboard-item/init', 'DashboardItemController@init');
Route::get('/admin/dashboard-item/edit', 'DashboardItemController@edit');
Route::get('/admin/dashboard-item/remove', 'DashboardItemController@remove');
Route::post('/admin/dashboard-item/save', 'DashboardItemController@save');

Route::get('/public/dashboard', 'DashboardController@index');




