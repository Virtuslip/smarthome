<?php

namespace App\Console\Commands;

use App\Drivers\XiaomiDriver;
use Illuminate\Console\Command;

class XiaomiSync extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'xiaomi:sync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Сервис синхронизации устройств Xiaomi';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $driver = new XiaomiDriver();
        $driver->Sync();
    }
}
