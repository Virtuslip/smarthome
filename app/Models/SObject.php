<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SObject extends Model
{
    protected $table = 'sobjects';
    protected $fillable = ['code', 'name', 'section_id', 'icon', 'uid', 'module_id', 'active'];

    public function module()
    {
        return $this->belongsTo('App\Models\Module', 'module_id', 'id');
    }

    public function data()
    {
        return $this->hasMany('App\Models\SObjectData', 'sobject_id', 'id');
    }
}
