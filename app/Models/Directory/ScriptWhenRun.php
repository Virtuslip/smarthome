<?php

namespace App\Models\Directory;

use Illuminate\Database\Eloquent\Model;

class ScriptWhenRun extends Model
{
    protected $table = 'd_script_when_run';
    protected $fillable = ['code', 'name'];
}
