<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Script extends Model
{
    protected $table = 'scripts';
    protected $fillable = ['code', 'name', 'action', 'when_run', 'shedule_token', 'shedule_time'];

    public function whenRun()
    {
        return $this->belongsTo('App\Models\Directory\ScriptWhenRun', 'when_run', 'id');
    }
}
