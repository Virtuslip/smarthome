<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Dashboard extends Model
{
    protected $table = 'dashboards';
    protected $fillable = ['name'];

    public function items()
    {
        return $this->hasMany('App\Models\DashboardItem', 'dashboard_id', 'id');
    }
}
