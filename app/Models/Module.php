<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    protected $table = 'modules';
    protected $fillable = ['code', 'name', 'active'];

    public function data()
    {
        return $this->hasMany('App\Models\ModuleSettings', 'module_id', 'id');
    }
}
