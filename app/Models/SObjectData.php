<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SObjectData extends Model
{
    protected $table = 'sobject_data';
    protected $fillable = ['code', 'name', 'sobject_id', 'type_id', 'script_id', 'value', 'is_readonly', 'module_id'];

    public function sobject()
    {
        return $this->belongsTo('App\Models\SObject', 'sobject_id', 'id');
    }

    public function type()
    {
        return $this->belongsTo('App\Models\DataType', 'type_id', 'id');
    }

    public function script()
    {
        return $this->belongsTo('App\Models\Script', 'script_id', 'id');
    }
}
