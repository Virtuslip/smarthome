<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DashboardDisplayType extends Model
{
    protected $table = 'dashboard_display_type';
    protected $fillable = ['code', 'name'];
}
