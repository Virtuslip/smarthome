<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ModuleSettings extends Model
{
    protected $table = 'module_settings';
    protected $fillable = ['code', 'name', 'value'];

    public function type()
    {
        return $this->belongsTo('App\Models\DataType', 'type_id', 'id');
    }
}
