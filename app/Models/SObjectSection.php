<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SObjectSection extends Model
{
    protected $table = 'sobject_section';
    protected $fillable = ['name', 'parent_id'];
}
