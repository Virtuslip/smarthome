<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DashboardItem extends Model
{
    protected $table = 'dashboard_item';
    protected $fillable = ['dashboard_id'];

    public function dashboard()
    {
        return $this->belongsTo('App\Models\Dashboard', 'dashboard_id', 'id');
    }

    public function object_item()
    {
        return $this->belongsTo('App\Models\SObjectData', 'object_item_id', 'id');
    }

    public function type(){
        return $this->belongsTo('App\Models\DashboardDisplayType', 'display_type', 'id');

    }
}
