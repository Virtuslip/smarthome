<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class SmartProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('Smart', 'App\Services\Smart');
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
