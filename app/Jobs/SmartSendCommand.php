<?php

namespace App\Jobs;

use App\Drivers\BroadlinkDriver;
use App\Drivers\XiaomiDriver;
use App\Models\SmartObject;
use App\Models\SmartObjectData;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class SmartSendCommand implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $deviceCode;
    protected $propertyCode;
    protected $value;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($deviceCode, $propertyCode, $value)
    {
        $this->deviceCode = $deviceCode;
        $this->propertyCode = $propertyCode;
        $this->value = $value;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $smartObject = SmartObject::where('code', $this->deviceCode)->with('data')->first();
        if (empty($smartObject)) {
            Log::error('Объект с кодом ' . $this->deviceCode . ' не найден');
        } else {
            $smartProp = SmartObjectData::where('code', $this->propertyCode)->where('smart_object_id', $smartObject->id)->first();
            if (empty($smartProp)) {
                Log::error('Свойство с кодом ' . $this->propertyCode . ' не найдено');
            } else {
                switch ($smartObject->module){
                    case 'xiaomi':
                        $driver = new XiaomiDriver();
                        $driver->SendCommand($smartObject, $smartProp, $this->value);
                        break;
                    case 'broadlink':
                        $driver = new BroadlinkDriver();
                        $driver->SendCommand($smartObject, $smartProp, $this->value);
                        break;
                }
            }
        }
    }
}
