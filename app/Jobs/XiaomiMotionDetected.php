<?php

namespace App\Jobs;
use App\Models\SObjectData;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class XiaomiMotionDetected implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $dataID;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($dataID)
    {
        $this->dataID = $dataID;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $smartObjectData = SObjectData::find($this->dataID);
        $smartObjectData->value = '';
        $smartObjectData->save();
    }
}
