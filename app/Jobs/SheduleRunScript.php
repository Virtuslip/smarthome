<?php

namespace App\Jobs;
use App\Models\Script;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SheduleRunScript implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $scriptID;
    private $token;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($scriptID, $token)
    {
        $this->scriptID = $scriptID;
        $this->token = $token;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $script = Script::find($this->scriptID);
        if($script->shedule_token == $this->token){
            eval($script->action);
        }
    }
}
