<?php

namespace App\Observers;

use App\Jobs\SheduleRunScript;
use App\Models\Script;
use App\Models\SObjectData;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class SObjectDataObserver
{
    /**
     * Handle the smart object data "created" event.
     *
     * @param \App\SmartObjectData $smartObjectData
     * @return void
     */
    public function created(SObjectData $smartObjectData)
    {
        //
    }

    /**
     * Handle the smart object data "updated" event.
     *
     * @param \App\Models\SObjectData $smartObjectData
     * @return void
     */
    public function updated(SObjectData $smartObjectData)
    {

        Log::info('X');
        if ($smartObjectData->script_id > 0) {
            $script = Script::where('id', $smartObjectData->script_id)->with('whenRun')->first();
            if($script->whenRun->code == 'delay'){
                Log::info('delay');
                $token = uniqid();
                $job = (new SheduleRunScript($smartObjectData->script_id, $token))->delay(Carbon::now()->addSeconds($script->shedule_time));
                dispatch($job);
                $script->shedule_token = $token;
                $script->save();
            }else {
                eval($script->action);
            }
        }
    }

    /**
     * Handle the smart object data "deleted" event.
     *
     * @param \App\SmartObjectData $smartObjectData
     * @return void
     */
    public function deleted(SObjectData $smartObjectData)
    {
        //
    }

    /**
     * Handle the smart object data "restored" event.
     *
     * @param \App\SmartObjectData $smartObjectData
     * @return void
     */
    public function restored(SObjectData $smartObjectData)
    {
        //
    }

    /**
     * Handle the smart object data "force deleted" event.
     *
     * @param \App\SmartObjectData $smartObjectData
     * @return void
     */
    public function forceDeleted(SObjectData $smartObjectData)
    {
        //
    }
}
