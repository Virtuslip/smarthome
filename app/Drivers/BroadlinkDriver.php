<?php


namespace App\Drivers;

use App\Models\DataType;
use App\Drivers\Broadlink\Broadlink;
use App\Drivers\Broadlink\SP2;
use App\Models\Module;
use App\Models\SObject;
use App\Models\SObjectData;
use Illuminate\Support\Facades\Log;

class BroadlinkDriver
{
    private static $dataTypes;

    public function Sync()
    {
        $module = Module::where('code', 'broadlink')->first();
        if(!empty($module) && $module->active){
            $types = DataType::all();
            foreach ($types as $type) {
                self::$dataTypes[$type['code']] = $type->id;
            }

            $devices = Broadlink::Discover();
            foreach ($devices as $bDevice) {
                $devType = $bDevice->devtype();
                $mac = $bDevice->mac();

                $smartObject = SObject::where('uid', $mac)->with('data')->first();
                if (empty($smartObject)) {
                    $smartObject = new SObject();
                    $smartObject->module_id = $module->id;
                    $smartObject->uid = $mac;
                    $smartObject->name = $bDevice->name();
                    $smartObject->code = 'broadlink_device_' . time();
                    $smartObject->save();
                }
                $smartObject->save();

                //Модель устройства
                if (empty($smartObject->data->firstWhere('code', 'devmodel'))) {
                    $model = new SObjectData();
                    $model->code = 'devmodel';
                    $model->sobject_id = $smartObject->id;
                    $model->name = 'Модель';
                    $model->type_id = self::$dataTypes['string'];
                    $model->value = $bDevice->devmodel();
                    $model->is_readonly = true;
                    $model->is_system = true;
                    $smartObject->data()->save($model);
                }

                //Тип устройства
                if (empty($smartObject->data->firstWhere('code', 'devtype'))) {
                    $model = new SObjectData();
                    $model->code = 'devtype';
                    $model->sobject_id = $smartObject->id;
                    $model->name = 'Тип';
                    $model->type_id = self::$dataTypes['string'];
                    $model->value = $devType;
                    $model->is_readonly = true;
                    $model->is_system = true;
                    $smartObject->data()->save($model);
                }

                //Адрес устройства
                $host = $smartObject->data->firstWhere('code', 'host');
                if (empty($host)) {
                    $host = new SObjectData();
                    $host->code = 'host';
                    $host->sobject_id = $smartObject->id;
                    $host->name = 'IP';
                    $host->type_id = self::$dataTypes['string'];
                    $host->is_readonly = true;
                    $model->is_system = true;
                }
                $host->value = $bDevice->host();

                $smartObject->data()->save($host);

                $mac = explode(':', $mac);
                foreach ($mac as &$val) {
                    $val = hexdec($val);
                }
                $mac = array_reverse($mac);
                switch ($devType) {
                    case '0x7547':
                        $data = $smartObject->data->firstWhere('code', 'power_state');
                        if (empty($data)) {
                            $data = new SObjectData();
                            $data->code = 'power_state';
                            $data->sobject_id = $smartObject->id;
                            $data->name = 'Статус';
                            $data->type_id = self::$dataTypes['switcher'];
                            $host->is_readonly = true;
                            $model->is_system = true;
                            $smartObject->data()->save($data);
                        }
                        $sp2 = new SP2($bDevice->host(), $mac);
                        $sp2->Auth();
                        $state = $sp2->Check_Power();
                        $data->value = $state['power_state'];
                        $data->save();
                        break;
                }
            }
        }
    }

    public function SendCommand(SObject $device, SObjectData $prop, $value)
    {
        $model = $device->data->firstWhere('code', 'devtype');
       switch ($model->value) {
            case '0x7547':
                $mac = explode(':', $device->uid);
                foreach ($mac as &$val) {
                    $val = hexdec($val);
                }
                $host = $device->data->firstWhere('code', 'host');
                $mac = array_reverse($mac);
                $sp2 = new SP2($host->value, $mac);
                $sp2->Auth();

                $sp2->Set_Power($value);
                $prop->value = $value;
                $prop->save();
                break;
        }
        return $prop;
    }
}
