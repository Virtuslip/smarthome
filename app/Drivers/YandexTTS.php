<?php

namespace App\Drivers;

use App\Models\SmartObject;
use App\Models\SmartObjectData;
use App\Models\SmartObjectProp;
use App\Models\SmartObjectPropType;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class YandexTTS
{
    public static function install()
    {
        $object = SmartObject::where('code', 'yandex_tts')->first();
        $dbRes = SmartObjectPropType::all();
        $objectParams = [];
        foreach ($dbRes as $item) {
            $objectParams[$item->code] = $item->id;
        }

        if (empty($object)) {
            $object = new SmartObject();
            $object->module = 'yandex';
            $object->sid = 'yandex_tts';
            $object->name = 'Яндекс TTS';
            $object->code = 'yandex_tts';
            $object->save();
        }


        SmartObjectProp::firstOrCreate(
            [
                'code' => 'token',
                'smart_object_id' => $object->id],
            [
                'smart_object_prop_type_id' => $objectParams['string'],
                'name' => 'Oath токен',
                'value' => '',
                'readonly' => false,
                'hidden' => false,
            ]
        );

        SmartObjectProp::firstOrCreate(
            [
                'code' => 'folder_id',
                'smart_object_id' => $object->id],
            [
                'smart_object_prop_type_id' => $objectParams['string'],
                'name' => 'ID папки консоли yandex',
                'value' => '',
                'readonly' => false,
                'hidden' => false,
            ]
        );
    }

    public static function speech($text)
    {
        $object = SmartObject::where('code', 'yandex_tts')->first();
        $oath = SmartObjectData::where('code', 'token_iam')->where('smart_object_id', $object->id)->first();
        $folderID = SmartObjectProp::where('code', 'folder_id')->where('smart_object_id', $object->id)->first();
        $url = "https://tts.api.cloud.yandex.net/speech/v1/tts:synthesize";
        $post = "text=" . urlencode($text) . "&lang=ru-RU&folderId=" . $folderID->value;
        $headers = ['Authorization: Bearer ' . $oath->value];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        if ($post !== false) {
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        }
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $response = curl_exec($ch);
        if (curl_errno($ch)) {
            Log::error(curl_error($ch));
        }
        if (curl_getinfo($ch, CURLINFO_HTTP_CODE) != 200) {
            $decodedResponse = json_decode($response, true);
            Log::error($decodedResponse["error_code"]);
            Log::error($decodedResponse["error_message"]);
        } else {
            Storage::disk('local')->put('speech.ogg', $response);
            //Storage::put('speech.ogg', $response);
//            File::put('path/to/file', 'file contents');
//            file_put_contents("/voices_cache/speech.ogg", $response);
        }
        curl_close($ch);
    }

    public static function GetIamToken()
    {
        $dbRes = SmartObjectPropType::all();
        $objectParams = [];
        foreach ($dbRes as $item) {
            $objectParams[$item->code] = $item->id;
        }
        $object = SmartObject::where('code', 'yandex_tts')->first();
        $oath = SmartObjectProp::where('code', 'token')->first();
        //$folderID = SmartObjectProp::where('code', 'folder_id')->first();
//        $oath = '95ebba06-4ef9-43a1-8f62-a6a07d54c56f';
        //$folderID = 'b1gd4b557s579kitvnqq';
        $url = 'https://iam.api.cloud.yandex.net/iam/v1/tokens';
        $post = "{\"yandexPassportOauthToken\":\"$oath->value\"}";
        $ch2 = curl_init();
        curl_setopt($ch2, CURLOPT_AUTOREFERER, TRUE);
        curl_setopt($ch2, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch2, CURLOPT_URL, $url);
        curl_setopt($ch2, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($ch2, CURLOPT_HEADER, false);
        curl_setopt($ch2, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch2, CURLOPT_SSL_VERIFYPEER, false);
        if ($post !== false) {
            curl_setopt($ch2, CURLOPT_POST, 1);
            curl_setopt($ch2, CURLOPT_POSTFIELDS, $post);
        }
        $res = curl_exec($ch2);
        curl_close($ch2);
        $res = json_decode($res);
        SmartObjectData::firstOrCreate(
            [
                'code' => 'token_iam',
                'smart_object_id' => $object->id],
            [
                'smart_object_prop_type_id' => $objectParams['string'],
                'name' => 'Iam токен',
                'value' => $res->iamToken,
                'readonly' => false,
                'hidden' => true,
            ]
        );
        SmartObjectData::firstOrCreate(
            [
                'code' => 'token_iam_expiried',
                'smart_object_id' => $object->id],
            [
                'smart_object_prop_type_id' => $objectParams['string'],
                'name' => 'Срок действия IAM токен',
                'value' => $res->expiresAt,
                'readonly' => false,
                'hidden' => true,
            ]
        );
    }
}
