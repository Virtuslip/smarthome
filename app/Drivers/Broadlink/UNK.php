<?php


namespace App\Drivers\Broadlink;


class UNK extends Broadlink{

    function __construct($h = "", $m = "", $p = 80, $d = 0x2712) {

        parent::__construct($h, $m, $p, $d);

    }

    public function some_action($params){//пример команды

        $packet = self::bytearray(16);
        $packet[0] = 0x02; //стартовый байт, определяющий действие (команда)
        $packet[4] = 1; // управляющий байт в команде
        $this->send_packet(0x6a, $packet);
    }

    public function some_req(){

        $packet = self::bytearray(16); //размер массива может быть другой...но как правило 16 или 48 байт
        $packet[0] = 0x01; //стартовый байт, определяющий действие (запрос)
        $response = $this->send_packet(0x6a, $packet);
        if (empty($response))
            return false;

        $err = hexdec(sprintf("%x%x", $response[0x23], $response[0x22]));

        if($err == 0){
            $enc_payload = array_slice($response, 0x38);

            if(count($enc_payload) > 0){

                $payload = $this->byte2array(self::aes128_cbc_decrypt($this->key(), $this->byte($enc_payload), $this->iv()));
                return $payload;
            }

        }

        return false;


    }

}
