<?php


namespace App\Drivers\Broadlink;


class MS1 extends Broadlink
{

    function __construct($h = "", $m = "", $p = 80, $d = 0x271F)
    {

        parent::__construct($h, $m, $p, $d);

    }

    public function send_str($ascii)
    {

        $packet = self::bytearray(16);
        $ascii = 'LEN:' . strlen($ascii) . chr(10) . $ascii;
        $hex = '';
        for ($i = 0; $i < strlen($ascii); $i++) {
            $byte = strtoupper(dechex(ord($ascii{$i})));
            $byte = str_repeat('0', 2 - strlen($byte)) . $byte;
            $hex .= $byte . ' ';
        }
        $hex_arr = explode(' ', $hex);
        $i = 0;
        foreach ($hex_arr as $hex_arr_byte) {
            $packet[$i] = '0x' . $hex_arr_byte;
            $i++;
        }

        $response = $this->send_packet(0x6a, $packet);
        if (empty($response))
            return false;

        $err = hexdec(sprintf("%x%x", $response[0x23], $response[0x22]));

        if ($err == 0) {
            $enc_payload = array_slice($response, 0x38);

            if (count($enc_payload) > 0) {
                $payload = $this->byte2array(self::aes128_cbc_decrypt($this->key(), $this->byte($enc_payload), $this->iv()));
                for ($i = 0; $i < count($payload); $i++) {
                    $payload[$i] = dechex($payload[$i]);
                    if (strlen($payload[$i]) == 1) $payload[$i] = '0' . $payload[$i];
                    if ($payload[$i] == '00') $payload[$i] = '';
                }
                $hex = implode('', $payload);
                $str = '';
                for ($i = 14; $i < strlen($hex); $i += 2) $str .= chr(hexdec(substr($hex, $i, 2)));
                return $str;
            }

        }

        return false;

    }

}
