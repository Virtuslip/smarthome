<?php


namespace App\Drivers\Broadlink;


class DOOYA extends Broadlink{

    function __construct($h = "", $m = "", $p = 80, $d = 0x2d) {

        parent::__construct($h, $m, $p, $d);

    }
    public function send_req($magic1=0x06, $magic2=0x5d){
        $data = array();
        $packet = self::bytearray(16);
        $packet[0] = 0x09;
        $packet[2] = 0xbb;
        $packet[3] = $magic1;
        $packet[4] = $magic2;
        $packet[9] = 0xfa;
        $packet[10] = 0x44;
        $response=$this->send_packet(0x6a, $packet);
        if (empty($response))
            return $data;

        $err = hexdec(sprintf("%x%x", $response[0x23], $response[0x22]));
        if($err == 0){
            $enc_payload = array_slice($response, 0x38);
            if(count($enc_payload) > 0){
                $payload = $this->byte2array(self::aes128_cbc_decrypt($this->key(), $this->byte($enc_payload), $this->iv()));
                $data = $payload[4];
            }
        }
        return $data;
    }
    public function get_level(){
        return $this->send_req();
    }
    public function set_level($level){
        $now_lvl=$this->get_level();
        if ($level!=$now_lvl) {
            if ($level==0) {
                $response=$this->send_req(0x02, 0x00); //закрыть
                return;
            } elseif ($level==100) {
                $response=$this->send_req(0x01, 0x00); //открыть
                return;
            }
            if ($now_lvl>$level) {
                $response=$this->send_req(0x02, 0x00);
                $action='close';
            } else {
                $response=$this->send_req(0x01, 0x00);
                $action='open';
            }
            if($action=='close') {
                while($now_lvl>$level) {
                    $now_lvl=$this->get_level();
                    usleep(200000);
                }
                $this->send_req(0x03, 0x00);
            } else {
                while($now_lvl<$level) {
                    $now_lvl=$this->get_level();
                    usleep(200000);
                }
                $this->send_req(0x03, 0x00);
            }
        }
    }
}
