<?php


namespace App\Drivers\Broadlink;

class SP2 extends Broadlink
{
    function __construct($h = "", $m = "", $p = 80, $d = 0x2712) {

        parent::__construct($h, $m, $p, $d);

    }

    public function Set_Power($state){

        $packet = self::bytearray(16);
        $packet[0] = 0x02;
        $packet[4] = (int)$state;

        $this->send_packet(0x6a, $packet);
    }

    public function Check_Power(){

        $packet = self::bytearray(16);
        $packet[0] = 0x01;
        $response = $this->send_packet(0x6a, $packet);


        if (empty($response))
            return false;

        $err = hexdec(sprintf("%x%x", $response[0x23], $response[0x22]));

        if($err == 0){
            $enc_payload = array_slice($response, 0x38);

            if(count($enc_payload) > 0){
                $payload = $this->byte2array(self::aes128_cbc_decrypt($this->key(), $this->byte($enc_payload), $this->iv()));
                if ($payload[0x4] & 0x01) $data['power_state'] = 1; else $data['power_state'] = 0;
                if ($payload[0x4] & 0x02) $data['light_state'] = 1; else $data['light_state'] = 0;	//for sp3
                return $data;
            }

        }

        return false;

    }

    public function Check_Energy(){

        $packet = self::bytearray(16);
        $packet[0x00] = 0x08;
        $packet[0x02] = 0xFE;
        $packet[0x03] = 0x01;
        $packet[0x04] = 0x05;
        $packet[0x05] = 0x01;
        $packet[0x09] = 0x2D;
        $response = $this->send_packet(0x6a, $packet);
        if (empty($response))
            return false;

        $err = hexdec(sprintf("%x%x", $response[0x23], $response[0x22]));

        if($err == 0){
            $enc_payload = array_slice($response, 0x38);

            if(count($enc_payload) > 0){
                $payload = $this->byte2array(self::aes128_cbc_decrypt($this->key(), $this->byte($enc_payload), $this->iv()));
                $data= (dechex($payload[0x7])*10000+dechex($payload[0x6])*100+dechex($payload[0x5]))/100;
                return $data;
            }

        }

        return false;

    }
    public function Check_Energy_SP2(){

        $packet = self::bytearray(16);
        $packet[0x00] = 0x04;
        $packet[0x04] = 0xF2;
        $packet[0x05] = 0x20;
        $packet[0x06] = 0x02;
        $response = $this->send_packet(0x6a, $packet);
        if (empty($response))
            return false;

        $err = hexdec(sprintf("%x%x", $response[0x23], $response[0x22]));

        if($err == 0){
            $enc_payload = array_slice($response, 0x38);

            if(count($enc_payload) > 0){
                $payload = $this->byte2array(self::aes128_cbc_decrypt($this->key(), $this->byte($enc_payload), $this->iv()));
                $data= (dechex($payload[0x4])*10000+dechex($payload[0x5])*100+dechex($payload[0x6]))/100;
                return $data;
            }

        }

        return false;

    }
}
