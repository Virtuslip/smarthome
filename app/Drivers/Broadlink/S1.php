<?php


namespace App\Drivers\Broadlink;


class S1 extends Broadlink
{

    function __construct($h = "", $m = "", $p = 80, $d = 0x2722)
    {

        parent::__construct($h, $m, $p, $d);

    }

    protected function sensors($payload)
    {

        $data = array();

        $data['col_sensors'] = $payload[0x04];
        for ($i = 0; $i < $data['col_sensors']; $i++) {
            $offset = 0x05 + $i * 0x53;
            $status = $payload[$offset + 0x00] * 256 + $payload[$offset + 0x01];
            $data[$i]['sensor_number'] = $payload[$offset + 0x02];
            $data[$i]['product_id'] = $payload[$offset + 0x04];
            $data[$i]['photo'] = 'http://jp-clouddb.ibroadlink.com/sensor/picture/' . $data[$i]['product_id'] . '.png';
            $data[$i]['location'] = '';
            switch ($data[$i]['product_id']) {
                case 0x21:
                    $data[$i]['product_type'] = 'Wall Motion Sensor';

                    if ($status & 0x10) {
                        $data[$i]['status'] = 1;
                        $data[$i]['status_val'] = constant('LANG_BRS1_PERSON_DETECTED');
                    } else {
                        $data[$i]['status'] = 0;
                        $data[$i]['status_val'] = constant('LANG_BRS1_NO_PERSON');
                    }

                    if ($status & 0x40) {
                        $data[$i]['batterylow'] = 1;
                    } else {
                        $data[$i]['batterylow'] = 0;
                    }
                    if ($status & 0x20) {
                        $data[$i]['tamper'] = 1;
                    } else {
                        $data[$i]['tamper'] = 0;
                    }
                    break;
                case 0x31:
                    $data[$i]['product_type'] = 'Door Sensor';
                    if ($status & 0x10) {
                        $data[$i]['status'] = 1;
                        $data[$i]['status_val'] = constant('LANG_BRS1_OPENED');
                    } else {
                        $data[$i]['status'] = 0;
                        $data[$i]['status_val'] = constant('LANG_BRS1_CLOSED');
                    }

                    if ($status & 0x40) {
                        $data[$i]['batterylow'] = 1;
                    } else {
                        $data[$i]['batterylow'] = 0;
                    }

                    if ($status & 0x20) {
                        $data[$i]['tamper'] = 1;
                    } else {
                        $data[$i]['tamper'] = 0;
                    }
                    break;
                case 0x91:
                    $data[$i]['product_type'] = 'Key Fob';
                    $data[$i]['status'] = $status;
                    switch ($status) {
                        case 0x0000:
                            $data[$i]['status_val'] = constant('LANG_BRS1_CANCEL_SOS');
                            break;
                        case 0x0010:
                            $data[$i]['status_val'] = constant('LANG_BRS1_DISARM');
                            break;
                        case 0x0020:
                            $data[$i]['status_val'] = constant('LANG_BRS1_ARMED_FULL');
                            break;
                        case 0x0040:
                            $data[$i]['status_val'] = constant('LANG_BRS1_ARMED_PART');
                            break;
                        case 0x0080:
                            $data[$i]['status_val'] = 'SOS';
                            break;
                        default:
                            $data[$i]['status_val'] = constant('LANG_BRS1_UNKNOWN') . $status;
                    }
                    break;
                // for future:
                case 0x40:
                    $data[$i]['product_type'] = 'Gaz Sensor';
                    switch ($status) {
                        case 0x0000:
                        case 0x0010:
                        default:
                            $data[$i]['status'] = constant('LANG_BRS1_UNKNOWN') . $status;
                    }
                    break;
                case 0x51:
                    $data[$i]['product_type'] = 'Fire Sensor';
                    switch ($status) {
                        case 0x0000:
                        case 0x0010:
                        default:
                            $data[$i]['status'] = constant('LANG_BRS1_UNKNOWN') . $status;
                    }
                    break;
                default:
                    $data[$i]['product_type'] = 'Unknown: ' . $data[$i]['product_id'];
            }
            $data[$i]['product_name'] = "";
            for ($j = $offset + 0x05; $j < $offset + 0x15; $j++) if (!$payload[$j]) $data[$i]['product_name'] .= chr($payload[$j]);
            $data[$i]['device_id'] = $payload[$offset + 0x1e] * 16777216 + $payload[$offset + 0x1d] * 65536 + $payload[$offset + 0x1c] * 256 + $payload[$offset + 0x1b];
            $data[$i]['s1_pwd'] = dechex($payload[$offset + 0x22]) . dechex($payload[$offset + 0x21]) . dechex($payload[$offset + 0x20]) . dechex($payload[$offset + 0x1f]);

            switch ($payload[$offset + 0x23]) {
                case 0x00:
                    $data[$i]['armFull'] = false;
                    $data[$i]['armPart'] = false;
                    break;
                case 0x02:
                    $data[$i]['armFull'] = true;
                    $data[$i]['armPart'] = false;
                    break;
                case 0x03:
                    $data[$i]['armFull'] = true;
                    $data[$i]['armPart'] = true;
                    break;
                default:
                    $data[$i]['armFull'] = true;
                    $data[$i]['armPart'] = false;
            }

            switch ($payload[$offset + 0x25]) {
                case 0x00:
                    $data[$i]['zone'] = 'Not specified';
                    break;
                case 0x01:
                    $data[$i]['zone'] = 'Living room';
                    break;
                case 0x02:
                    $data[$i]['zone'] = 'Main bedroom';
                    break;
                case 0x03:
                    $data[$i]['zone'] = 'Secondary room 1';
                    break;
                case 0x04:
                    $data[$i]['zone'] = 'Secondary room 2';
                    break;
                case 0x05:
                    $data[$i]['zone'] = 'Kitchen';
                    break;
                case 0x06:
                    $data[$i]['zone'] = 'Bathroom';
                    break;
                case 0x07:
                    $data[$i]['zone'] = 'Veranda';
                    break;
                case 0x08:
                    $data[$i]['zone'] = 'Garage';
                    break;
                default:
                    $data[$i]['zone'] = constant('LANG_BRS1_UNKNOWN') . $payload[$offset + 0x2b];
            }

            $data[$i]['delay_online'] = $payload[$offset + 0x39] * 256 + $payload[$offset + 0x38];
            $data[$i]['delay_battery'] = $payload[$offset + 0x41] * 256 + $payload[$offset + 0x40];
            $data[$i]['delay_tamper_switch'] = $payload[$offset + 0x49] * 256 + $payload[$offset + 0x48];
            $data[$i]['delay_detect'] = $payload[$offset + 0x50] * 256 + $payload[$offset + 0x4f];
        }
        return $data;
    }

    public function Check_Sensors()
    {

        $data = array();

        $packet = self::bytearray(16);
        $packet[0] = 0x06;

        $response = $this->send_packet(0x6a, $packet);
        if (empty($response))
            return $data;

        $err = hexdec(sprintf("%x%x", $response[0x23], $response[0x22]));

        if ($err == 0) {
            $enc_payload = array_slice($response, 0x38);
            if (count($enc_payload) > 0) {
                $payload = $this->byte2array(self::aes128_cbc_decrypt($this->key(), $this->byte($enc_payload), $this->iv()));
                $data = $this->sensors($payload);
            }
        }
        return $data;
    }

    public function Check_Status()
    {

        $data = array();

        $packet = self::bytearray(16);
        $packet[0] = 0x12;

        $response = $this->send_packet(0x6a, $packet);
        if (empty($response))
            return $data;

        $err = hexdec(sprintf("%x%x", $response[0x23], $response[0x22]));

        if ($err == 0) {
            $enc_payload = array_slice($response, 0x38);
            if (count($enc_payload) > 0) {
                $payload = $this->byte2array(self::aes128_cbc_decrypt($this->key(), $this->byte($enc_payload), $this->iv()));
                $data['status'] = $payload[0x04];
                $data['delay_time_m'] = $payload[0x08];
                $data['delay_time_s'] = $payload[0x09];
                $data['alarm_buzzing'] = $payload[0x0a];
                $data['alarm_buzzing_duration'] = $payload[0x0b];
                $data['beep_mute'] = $payload[0x0d];
                $data['alarm_detector'] = $payload[0x28];
                switch ($data['status']) {
                    case 0x00:
                        $data['status_val'] = constant('LANG_BRS1_DISARM');
                        break;
                    case 0x01:
                        $data['status_val'] = constant('LANG_BRS1_PART');
                        break;
                    case 0x02:
                        $data['status_val'] = constant('LANG_BRS1_FULL');
                        break;
                    default:
                        $data['status'] = constant('LANG_BRS1_UNKNOWN') . $data['status'];
                }
            }
        }
        return $data;
    }

    public function Set_Arm($params)
    {

        $data = array();

        $packet = self::bytearray(48);

        $packet[0x00] = 0x11;
        $packet[0x04] = $params['status']; //2 - full, 1 - part, 0 - disarm
        $packet[0x08] = $params['delay_time_m'];
        $packet[0x09] = $params['delay_time_s'];
        $packet[0x0a] = $params['alarm_buzzing'];
        $packet[0x0b] = $params['alarm_buzzing_duration'];
        $packet[0x0d] = $params['beep_mute'];
        $packet[0x28] = $params['alarm_detector'];

        $response = $this->send_packet(0x6a, $packet);
        if (empty($response))
            return $data;

        $err = hexdec(sprintf("%x%x", $response[0x23], $response[0x22]));

        if ($err == 0) {
            $enc_payload = array_slice($response, 0x38);
            if (count($enc_payload) > 0) {
                $payload = $this->byte2array(self::aes128_cbc_decrypt($this->key(), $this->byte($enc_payload), $this->iv()));
                $data['status'] = $payload[0x04];
                $data['delay_time_m'] = $payload[0x08];
                $data['delay_time_s'] = $payload[0x09];
                $data['alarm_buzzing'] = $payload[0x0a];
                $data['alarm_buzzing_duration'] = $payload[0x0b];
                $data['beep_mute'] = $payload[0x0d];
                $data['alarm_detector'] = $payload[0x28];
                switch ($data['status']) {
                    case 0x00:
                        $data['status_val'] = constant('LANG_BRS1_DISARM');;
                        break;
                    case 0x01:
                        $data['status_val'] = constant('LANG_BRS1_PART');
                        break;
                    case 0x02:
                        $data['status_val'] = constant('LANG_BRS1_FULL');
                        break;
                    default:
                        $data['status'] = constant('LANG_BRS1_UNKNOWN') . $data['status'];
                }
            }
        }
        return $data;
    }

    public function Add_Sensor($serialnumb)
    {

        $data = array();

        $serial[0] = mb_strtoupper($serialnumb[0] . $serialnumb[1], "UTF-8");
        if ($serial[0] != 'BL') {
            return false;
        }
        for ($i = 2; $i < strlen($serialnumb) - 1; $i += 2) {
            $serial[$i / 2] = hexdec($serialnumb[$i] . $serialnumb[$i + 1]);
        }

        $packet = self::bytearray(96);
        $packet[0x00] = 0x07;
        $packet[0x05] = $serial[2];
        $packet[0x06] = $serial[3];
        switch ($serial[3]) {
            case 0x21:    //http://jp-clouddb.ibroadlink.com/sensor/picture/33.png /35.png and /36.png
                $packet[0x07] = ord('W');
                $packet[0x08] = ord('a');
                $packet[0x09] = ord('l');
                $packet[0x0A] = ord('l');
                $packet[0x0B] = ord(' ');
                $packet[0x0C] = ord('M');
                $packet[0x0D] = ord('o');
                $packet[0x0E] = ord('t');
                $packet[0x0F] = ord('i');
                $packet[0x10] = ord('o');
                $packet[0x11] = ord('n');
                $packet[0x12] = ord(' ');
                $packet[0x13] = ord('S');
                $packet[0x14] = ord('e');
                $packet[0x15] = ord('n');
                $packet[0x16] = ord('s');
                $packet[0x17] = ord('o');
                $packet[0x18] = ord('r');
                // ..0x1C - zeros
                break;
            case 0x31:    //http://jp-clouddb.ibroadlink.com/sensor/picture/49.png
                $packet[0x07] = ord('D');
                $packet[0x08] = ord('o');
                $packet[0x09] = ord('o');
                $packet[0x0A] = ord('r');
                $packet[0x0B] = ord(' ');
                $packet[0x0C] = ord('S');
                $packet[0x0D] = ord('e');
                $packet[0x0E] = ord('n');
                $packet[0x0F] = ord('s');
                $packet[0x10] = ord('o');
                $packet[0x11] = ord('r');
                // ..0x1C - zeros
                break;
            case 0x40:    //http://jp-clouddb.ibroadlink.com/sensor/picture/64.png
                $packet[0x07] = ord('G');
                $packet[0x08] = ord('a');
                $packet[0x09] = ord('z');
                $packet[0x0A] = ord(' ');
                $packet[0x0B] = ord('S');
                $packet[0x0C] = ord('e');
                $packet[0x0D] = ord('n');
                $packet[0x0E] = ord('s');
                $packet[0x0F] = ord('o');
                $packet[0x10] = ord('r');
                // ..0x1C - zeros
                break;
            case 0x51:    //http://jp-clouddb.ibroadlink.com/sensor/picture/81.png
                $packet[0x07] = ord('F');
                $packet[0x08] = ord('i');
                $packet[0x09] = ord('r');
                $packet[0x0A] = ord('e');
                $packet[0x0B] = ord(' ');
                $packet[0x0C] = ord('S');
                $packet[0x0D] = ord('e');
                $packet[0x0E] = ord('n');
                $packet[0x0F] = ord('s');
                $packet[0x10] = ord('o');
                $packet[0x11] = ord('r');
                // ..0x1C - zeros
                break;
            case 0x91:    //http://jp-clouddb.ibroadlink.com/sensor/picture/145.png
                $packet[0x07] = ord('K');
                $packet[0x08] = ord('e');
                $packet[0x09] = ord('y');
                $packet[0x0A] = ord(' ');
                $packet[0x0B] = ord('F');
                $packet[0x0C] = ord('o');
                $packet[0x0D] = ord('b');
                // ..0x1C - zeros
                break;
            default:    //http://jp-clouddb.ibroadlink.com/sensor/picture/224.png /239.png
                $packet[0x07] = ord('U');
                $packet[0x08] = ord('n');
                $packet[0x09] = ord('k');
                $packet[0x0A] = ord('n');
                $packet[0x0B] = ord('o');
                $packet[0x0C] = ord('w');
                $packet[0x0D] = ord('n');
            // ..0x1C - zeros
        }
        $packet[0x1D] = $serial[4];
        $packet[0x1E] = $serial[5];
        $packet[0x1F] = $serial[6];
        $packet[0x20] = $serial[7];
        switch ($serial[3]) {
            case 0x21:    //s1_pwd = 0x774eecd6
                $packet[0x21] = 0xd6;
                $packet[0x22] = 0xec;
                $packet[0x23] = 0x4e;
                $packet[0x24] = 0x77;
                break;
            case 0x31:    //s1_pwd = 0x95a1faf1
                $packet[0x21] = 0xf1;
                $packet[0x22] = 0xfa;
                $packet[0x23] = 0xa1;
                $packet[0x24] = 0x95;
                break;
            case 0x91:    //s1_pwd = 0x5d6f7647
                $packet[0x21] = 0x47;
                $packet[0x22] = 0x76;
                $packet[0x23] = 0x6f;
                $packet[0x24] = 0x5d;
                break;
        }
        $packet[0x25] = 0x02;    //0x00 = Full-arm disabled, Part-arm disabled
        //0x02 = Full-arm enabled, Part-arm disabled
        //0x03 = Full-arm enabled, Part-arm enabled
        //$packet[0x26] = 0x00;
        if (($serial[3] == 0x21) || ($serial[3] == 0x31)) {
            $packet[0x27] = 0x00;    //0x00 = Not specified
            //0x01 = Living room
            //0x02 = Main bedroom
            //0x03 = Secondary room 1
            //0x04 = Secondary room 2
            //0x05 = Kitchen
            //0x06 = Bathroom
            //0x07 = Veranda
            //0x08 = Garage
        }
        if ($serial[3] == 0x31) {
            $packet[0x28] = 0x00;    //0x00 = Drawer (for "Door Sensor")
            //0x01 = Door
            //0x02 = Window
        }
        //0x29..0x34:	zeros
        //Online Status
        $packet[0x35] = 0x1f;
        if ($serial[3] == 0x91) {
            $packet[0x36] = 0x01;
            //$packet[0x37] = 0x00;
            //$packet[0x38] = 0x00;
            $packet[0x39] = 0x0a;
        }
        if ($serial[3] == 0x31) {
            $packet[0x3a] = 0x2d;    //Delay time 45 sec (0x00 0x2D)
            $packet[0x3b] = 0x00;    //Delay time 45 sec (0x00 0x2D)
        }
        //$packet[0x3c] = 0x00;

        //Battery
        $packet[0x3d] = 0x1e;
        if (($serial[3] == 0x21) || ($serial[3] == 0x31)) {
            $packet[0x3e] = 0x08;
        }
        //$packet[0x3f] = 0x00;
        //$packet[0x40] = 0x00;
        //$packet[0x41] = 0x00;
        $packet[0x42] = 0x00;    //Delay time 0 sec (0x00 0x00)
        $packet[0x43] = 0x00;    //Delay time 0 sec (0x00 0x00)
        //$packet[0x44] = 0x00;

        //Tamper Switch
        $packet[0x45] = 0x1d;
        if (($serial[3] == 0x21) || ($serial[3] == 0x31)) {
            $packet[0x46] = 0x08;
        }
        //$packet[0x47] = 0x00;
        //$packet[0x48] = 0x00;
        //$packet[0x49] = 0x00;
        $packet[0x4a] = 0x00;    //Delay time 0 sec (0x00 0x00)
        $packet[0x4b] = 0x00;    //Delay time 0 sec (0x00 0x00)
        //$packet[0x4c] = 0x00;

        //Detected Status
        $packet[0x4d] = 0x1c;
        if (($serial[3] == 0x21) || ($serial[3] == 0x31)) {
            $packet[0x4e] = 0x0b;
        }
        //$packet[0x4f] = 0x00;
        //$packet[0x50] = 0x00;
        //$packet[0x51] = 0x00;
        if ($serial[3] == 0x21) {
            $packet[0x52] = 0x68;    //Delay time 6 min (0x01 0x68)
            $packet[0x53] = 0x01;    //Delay time 6 min (0x01 0x68)
        }
        //0x54..0x5f:	zeros

        $response = $this->send_packet(0x6a, $packet);
        if (empty($response))
            return $data;

        $err = hexdec(sprintf("%x%x", $response[0x23], $response[0x22]));

        if ($err == 0) {
            $enc_payload = array_slice($response, 0x38);
            if (count($enc_payload) > 0) {
                $payload = $this->byte2array(self::aes128_cbc_decrypt($this->key(), $this->byte($enc_payload), $this->iv()));
                $data = $this->sensors($payload);
            }
        }
        return $data;
    }


}
