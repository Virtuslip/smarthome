<?php


namespace App\Drivers\Broadlink;



class SP1 extends Broadlink
{
    function __construct($h = "", $m = "", $p = 80, $d = 0x2712) {

        parent::__construct($h, $m, $p, $d);

    }

    public function Set_Power($state){

        $packet = self::bytearray(4);
        $packet[0] = $state;

        $this->send_packet(0x66, $packet);
    }
}
