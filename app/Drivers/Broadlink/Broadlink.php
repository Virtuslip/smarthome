<?php


namespace App\Drivers\Broadlink;
use Illuminate\Support\Facades\Log;

class Broadlink
{
    protected $name;
    protected $host;
    protected $port = 80;
    protected $mac;
    protected $reply;
    protected $timeout = 10;
    protected $count;
    protected $key = array(0x09, 0x76, 0x28, 0x34, 0x3f, 0xe9, 0x9e, 0x23, 0x76, 0x5c, 0x15, 0x13, 0xac, 0xcf, 0x8b, 0x02);
    protected $iv = array(0x56, 0x2e, 0x17, 0x99, 0x6d, 0x09, 0x3d, 0x28, 0xdd, 0xb3, 0xba, 0x69, 0x5a, 0x2e, 0x6f, 0x58);
    protected $id = array(0, 0, 0, 0);
    protected $devtype;
    public $ping_response;
    public $ping_time = 'down';
    public $ping_status;

    function __construct($h = "", $m = "", $p = 80, $d = 0)
    {

        $this->host = $h;
        $this->port = $p;
        $this->devtype = is_string($d) ? hexdec($d) : $d;

        if (is_array($m)) {

            $this->mac = $m;
        } else {

            $this->mac = array();
            $mac_str_array = explode(':', $m);

            foreach (array_reverse($mac_str_array) as $value) {
                array_push($this->mac, $value);
            }

        }


        $this->count = rand(0, 0xffff);

    }

    function __destruct()
    {


    }

    protected static function aes128_cbc_encrypt($key, $data, $iv)
    {
        $data = str_pad($data, ceil(strlen($data) / 16) * 16, chr(0), STR_PAD_RIGHT);
        return openssl_encrypt($data, 'AES-128-CBC', $key, OPENSSL_RAW_DATA | OPENSSL_ZERO_PADDING, $iv);
    }

    protected static function aes128_cbc_decrypt($key, $data, $iv)
    {
        return rtrim(openssl_decrypt($data, 'AES-128-CBC', $key, OPENSSL_RAW_DATA | OPENSSL_ZERO_PADDING, $iv), chr(0));
    }

    protected static function generateCsv($data)
    {
        # Generate CSV data from array
        $fh = fopen('php://temp', 'rw');    # don't create a file, attempt
        # to use memory instead
        # write out the headers
        fputcsv($fh, array_keys(current($data)));

        # write out the data
        foreach ($data as $row) {
            fputcsv($fh, $row);
        }
        rewind($fh);
        $csv = stream_get_contents($fh);
        fclose($fh);

        return $csv;
    }

    public static function model($devtype, $needle = 'type')
    {

        $type = "Unknown";
        $model = "Unknown";
        if (is_string($devtype)) $devtype = hexdec($devtype);

        switch ($devtype) {
            case 0:
                $model = "SP1";
                $type = 0;
                break;
            case 0x2711:
                $model = "SP2";
                $type = 1;
                break;
            case 0x2719:
            case 0x7919:
            case 0x271a:
            case 0x791a:
                $model = "Honeywell SP2";
                $type = 1;
                break;
            case 0x2720:
                $model = "SPMini";
                $type = 1;
                break;
            case 0x753e:
                $model = "SP3";
                $type = 1;
                break;
            case 0x2728:
                $model = "SPMini2";
                $type = 1;
                break;
            case 0x2733:
            case 0x273e:
            case 0x7539:
            case 0x754e:
            case 0x753d:
            case 0x7536:
                $model = "OEM branded SPMini";
                $type = 1;
                break;
            case 0x7540:
                $model = "MP2";
                $type = 1;
                break;
            case 0x7530:
            case 0x7918:
            case 0x7549:
                $model = "OEM branded SPMini2";
                $type = 1;
                break;
            case 0x2736:
                $model = "SPMiniPlus";
                $type = 1;
                break;
            case 0x947c:
                $model = "SPMiniPlus2";
                $type = 1;
                break;
            case 0x7547:
                $model = "SC1 WiFi Box";
                $type = 1;
                break;
            case 0x947a:
            case 0x9479:
                $model = "SP3S";
                $type = 1;
                break;
            case 0x2710:
                $model = "RM1";
                $type = 2;
                break;
            case 0x2712:
                $model = "RM2";
                $type = 2;
                break;
            case 0x2737:
                $model = "RM Mini";
                $type = 2;
                break;
            case 0x27a2:
                $model = "RM Mini R2";
                $type = 2;
                break;
            case 0x273d:
                $model = "RM Pro Phicomm";
                $type = 2;
                break;
            case 0x2783:
                $model = "RM2 Home Plus";
                $type = 2;
                break;
            case 0x277c:
                $model = "RM2 Home Plus GDT";
                $type = 2;
                break;
            case 0x272a:
                $model = "RM2 Pro Plus";
                $type = 2;
                break;
            case 0x2787:
                $model = "RM2 Pro Plus2";
                $type = 2;
                break;
            case 0x279d:
                $model = "RM2 Pro Plus3";
                $type = 2;
                break;
            case 0x2797:
                $model = "RM2 Pro Plus HYC";
                $type = 2;
                break;
            case 0x278b:
                $model = "RM2 Pro Plus BL";
                $type = 2;
                break;
            case 0x27a1:
            case 0x27a9:
                $model = "RM2 Pro Plus R1";
                $type = 2;
                break;
            case 0x278f:
                $model = "RM Mini Shate";
                $type = 2;
                break;
            case 0x27c2:
                $model = "RM3 mini";
                $type = 2;
                break;
            case 0x2714:
            case 0x27a3:
                $model = "A1";
                $type = 3;
                break;
            case 0x4EB5:
                $model = "MP1";
                $type = 4;
                break;
            case 0x271F:
                $model = "MS1";
                $type = 5;
                break;
            case 0x2722:
                $model = "S1";
                $type = 6;
                break;
            case 0x273c:
                $model = "S1 Phicomm";
                $type = 6;
                break;
            case 0x4f34:
            case 0x4f35:
            case 0x4f36:
                $model = "TW2 Switch";
                $type = 1;
                break;
            case 0x4ee6:
            case 0x4eee:
            case 0x4eef:
                $model = "NEW Switch";
                $type = 1;
                break;
            case 0x271b:
            case 0x271c:
                $model = "Honyar switch";
                $type = 1;
                break;
            case 0x2721:
                $model = "Camera";
                $type = 100;
                break;
            case 0x42:
            case 0x4e62:
                $model = "DEYE HUMIDIFIER";
                $type = 100;
                break;
            case 0x2d:
            case 0x4f42:
            case 0x4e4d:
                $model = "DOOYA CURTAIN";
                $type = 7;
                break;
            case 0x2723:
            case 0x4eda:
                $model = "HONYAR MS";
                $type = 100;
                break;
            case 0x2727:
            case 0x2726:
            case 0x2724:
            case 0x2725:
                $model = "HONYAR SL";
                $type = 100;
                break;
            case 0x4c:
            case 0x4e6c:
                $model = "MFRESH AIR";
                $type = 100;
                break;
            case 0x271e:
            case 0x2746:
                $model = "PLC (TW_ROUTER)";
                $type = 100;
                break;
            case 0x2774:
            case 0x2742:
            case 0x4e20:
                $model = "MIN/MAX AP/OEM";
                $type = 100;
                break;
            case 0x4e69:
                $model = "LIGHTMATES";
                $type = 100;
                break;
            case 0x4ead:
                $model = "HYSEN";
                $type = 8;
                break;
            default:
                break;
        }

        if ($needle == 'model') {
            return $model;
        } else {
            return $type;
        }
    }

    public static function CreateDevice($h = "", $m = "", $p = 80, $d = 0)
    {

        switch (self::model($d)) {
            case 0:
                return new SP1($h, $m, $p, $d);
                break;
            case 1:
                return new SP2($h, $m, $p, $d);
                break;
            case 2:
                return new RM($h, $m, $p, $d);
                break;
            case 3:
                return new A1($h, $m, $p, $d);
                break;
            case 4:
                return new MP1($h, $m, $p, $d);
                break;
            case 5:
                return new MS1($h, $m, $p, $d);
                break;
            case 6:
                return new S1($h, $m, $p, $d);
                break;
            case 7:
                return new DOOYA($h, $m, $p, $d);
                break;
            case 8:
                return new HYSEN($h, $m, $p, $d);
                break;
            case 100:
                return new UNK($h, $m, $p, $d);
                break;
            default:
        }

        return NULL;
    }

    protected function key()
    {
        return implode(array_map("chr", $this->key));
    }

    protected function iv()
    {
        return implode(array_map("chr", $this->iv));
    }

    public function mac()
    {

        $mac = "";

        foreach ($this->mac as $value) {
            $mac = sprintf("%02x", $value) . ':' . $mac;
        }

        return substr($mac, 0, strlen($mac) - 1);
    }

    public function host()
    {
        return $this->host;
    }

    public function name()
    {
        return $this->name;
    }

    public function devtype()
    {
        return sprintf("0x%x", $this->devtype);
    }

    public function devmodel()
    {
        return self::model($this->devtype, 'model');
    }


    protected static function bytearray($size)
    {

        $packet = array();

        for ($i = 0; $i < $size; $i++) {
            $packet[$i] = 0;
        }

        return $packet;
    }

    protected static function byte2array($data)
    {

        return array_merge(unpack('C*', $data));
    }

    protected static function byte($array)
    {

        return implode(array_map("chr", $array));
    }

    public static function Discover()
    {

        $devices = array();

        $s = socket_create(AF_INET, SOCK_DGRAM, SOL_UDP);
        socket_connect($s, '8.8.8.8', 53);  // connecting to a UDP address doesn't send packets
        socket_getsockname($s, $local_ip_address, $port);
        @socket_shutdown($s, 2);
        socket_close($s);

        $cs = socket_create(AF_INET, SOCK_DGRAM, SOL_UDP);

        if (!$cs) {
            return $devices;
        }

        socket_set_option($cs, SOL_SOCKET, SO_REUSEADDR, 1);
        socket_set_option($cs, SOL_SOCKET, SO_BROADCAST, 1);
        socket_set_option($cs, SOL_SOCKET, SO_RCVTIMEO, array('sec' => 1, 'usec' => 0));
        socket_bind($cs, 0, 0);

        $address = explode('.', $local_ip_address);
        $packet = self::bytearray(0x30);

        $timezone = (int)intval(date("Z")) / -3600;
        $year = date("Y");

        if ($timezone < 0) {
            $packet[0x08] = 0xff + $timezone - 1;
            $packet[0x09] = 0xff;
            $packet[0x0a] = 0xff;
            $packet[0x0b] = 0xff;
        } else {
            $packet[0x08] = $timezone;
            $packet[0x09] = 0;
            $packet[0x0a] = 0;
            $packet[0x0b] = 0;
        }
        $packet[0x0c] = $year & 0xff;
        $packet[0x0d] = $year >> 8;
        $packet[0x0e] = intval(date("i"));
        $packet[0x0f] = intval(date("H"));
        $subyear = substr($year, 2);
        $packet[0x10] = intval($subyear);
        $packet[0x11] = intval(date('N'));
        $packet[0x12] = intval(date("d"));
        $packet[0x13] = intval(date("m"));
        $packet[0x18] = intval($address[0]);
        $packet[0x19] = intval($address[1]);
        $packet[0x1a] = intval($address[2]);
        $packet[0x1b] = intval($address[3]);
        $packet[0x1c] = $port & 0xff;
        $packet[0x1d] = $port >> 8;
        $packet[0x26] = 6;

        $checksum = 0xbeaf;
        for ($i = 0; $i < sizeof($packet); $i++) {
            $checksum += $packet[$i];
        }
        $checksum = $checksum & 0xffff;

        $packet[0x20] = $checksum & 0xff;
        $packet[0x21] = $checksum >> 8;

        @socket_sendto($cs, self::byte($packet), sizeof($packet), 0, '255.255.255.255', 80);
        while (@socket_recvfrom($cs, $response, 2048, 0, $from, $port)) {

            $host = '';
            $responsepacket = self::byte2array($response);
            $devtype = hexdec(sprintf("%x%x", $responsepacket[0x35], $responsepacket[0x34]));
            $host_array = array_slice($responsepacket, 0x36, 4);
            $mac = array_slice($responsepacket, 0x3a, 6);
            if (array_slice($responsepacket, 0, 8) !== array(0x5a, 0xa5, 0xaa, 0x55, 0x5a, 0xa5, 0xaa, 0x55)) {
                $host_array = array_reverse($host_array);
            }

            foreach ($host_array as $ip) {
                $host .= $ip . ".";
            }

            $host = substr($host, 0, strlen($host) - 1);
            $device = Broadlink::CreateDevice($host, $mac, 80, $devtype);

            if ($device != NULL) {
                $device->name = str_replace(array("\0", "\2"), '', Broadlink::byte(array_slice($responsepacket, 0x40)));
                array_push($devices, $device);
            }
        }

        @socket_shutdown($cs, 2);
        socket_close($cs);

        return $devices;
    }

    function send_packet($command, $payload)
    {

        $cs = socket_create(AF_INET, SOCK_DGRAM, SOL_UDP);

        if (!$cs) {
            return array();
        }

        socket_set_option($cs, SOL_SOCKET, SO_REUSEADDR, 1);
        socket_set_option($cs, SOL_SOCKET, SO_BROADCAST, 1);
        socket_bind($cs, 0, 0);

        $this->count = ($this->count + 1) & 0xffff;

        $packet = $this->bytearray(0x38);
        $packet[0x00] = 0x5a;
        $packet[0x01] = 0xa5;
        $packet[0x02] = 0xaa;
        $packet[0x03] = 0x55;
        $packet[0x04] = 0x5a;
        $packet[0x05] = 0xa5;
        $packet[0x06] = 0xaa;
        $packet[0x07] = 0x55;
        $packet[0x24] = 0x2a;
        $packet[0x25] = 0x27;
        $packet[0x26] = $command;
        $packet[0x28] = $this->count & 0xff;
        $packet[0x29] = $this->count >> 8;
        $packet[0x2a] = $this->mac[0];
        $packet[0x2b] = $this->mac[1];
        $packet[0x2c] = $this->mac[2];
        $packet[0x2d] = $this->mac[3];
        $packet[0x2e] = $this->mac[4];
        $packet[0x2f] = $this->mac[5];
        $packet[0x30] = $this->id[0];
        $packet[0x31] = $this->id[1];
        $packet[0x32] = $this->id[2];
        $packet[0x33] = $this->id[3];

        $checksum = 0xbeaf;
        for ($i = 0; $i < sizeof($payload); $i++) {
            $checksum += $payload[$i];
            $checksum = $checksum & 0xffff;
        }
        $aes = $this->byte2array(self::aes128_cbc_encrypt($this->key(), $this->byte($payload), $this->iv()));

        $packet[0x34] = $checksum & 0xff;
        $packet[0x35] = $checksum >> 8;

        for ($i = 0; $i < sizeof($aes); $i++) {
            array_push($packet, $aes[$i]);
        }

        $checksum = 0xbeaf;
        for ($i = 0; $i < sizeof($packet); $i++) {
            $checksum += (int)$packet[$i];
            $checksum = $checksum & 0xffff;
        }


        $packet[0x20] = $checksum & 0xff;
        $packet[0x21] = $checksum >> 8;

        $from = '';
        socket_sendto($cs, $this->byte($packet), sizeof($packet), 0, $this->host, $this->port);
        socket_set_option($cs, SOL_SOCKET, SO_RCVTIMEO, array('sec' => $this->timeout, 'usec' => 0));
        $ret = socket_recvfrom($cs, $response, 2048, 0, $from, $port);
        @socket_shutdown($cs, 2);
        socket_close($cs);

        if ($ret === FALSE)
            return array();

        return $this->byte2array($response);
    }

    public function Auth($id_authorized = null, $key_authorized = null)
    {
        if (!isset($id_authorized) || !isset($key_authorized)) {
            $payload = $this->bytearray(0x50);
            $payload[0x04] = 0x31;
            $payload[0x05] = 0x31;
            $payload[0x06] = 0x31;
            $payload[0x07] = 0x31;
            $payload[0x08] = 0x31;
            $payload[0x09] = 0x31;
            $payload[0x0a] = 0x31;
            $payload[0x0b] = 0x31;
            $payload[0x0c] = 0x31;
            $payload[0x0d] = 0x31;
            $payload[0x0e] = 0x31;
            $payload[0x0f] = 0x31;
            $payload[0x10] = 0x31;
            $payload[0x11] = 0x31;
            $payload[0x12] = 0x31;
            $payload[0x1e] = 0x01;
            $payload[0x2d] = 0x01;
            $payload[0x30] = ord('T');
            $payload[0x31] = ord('e');
            $payload[0x32] = ord('s');
            $payload[0x33] = ord('t');
            $payload[0x34] = ord(' ');
            $payload[0x35] = ord(' ');
            $payload[0x36] = ord('1');

            $response = $this->send_packet(0x65, $payload);
            if (empty($response))
                return false;

            $enc_payload = array_slice($response, 0x38);

            $payload = $this->byte2array(self::aes128_cbc_decrypt($this->key(), $this->byte($enc_payload), $this->iv()));

            $this->id = array_slice($payload, 0x00, 4);
            $this->key = array_slice($payload, 0x04, 16);

            $data['id'] = $this->id;
            $data['key'] = $this->key;
            $data['time'] = time();

            return $data;
        } else {
            $this->id = $id_authorized;
            $this->key = $key_authorized;
        }
        return true;
    }

//    public static function Cloud($nickname = "", $userid = "", $loginsession = "") {
//
//        return new Cloud($nickname, $userid, $loginsession);
//
//    }

    protected static function str2hex_array($str)
    {

        $str_arr = str_split(strToUpper($str), 2);
        $str_hex = array();
        for ($i = 0; $i < count($str_arr); $i++) {
            $ord1 = ord($str_arr[$i][0]) - 48;
            $ord2 = ord($str_arr[$i][1]) - 48;
            if ($ord1 > 16) $ord1 = $ord1 - 7;
            if ($ord2 > 16) $ord2 = $ord2 - 7;
            $str_hex[$i] = $ord1 * 16 + $ord2;
        }
        return $str_hex;
    }

    public function ping()
    {

        $timeout = 500;
        $precision = 5;
        $udp_port = 33439;
        $request = 'broadlink-monitoring-system';

        switch (self::model($this->devtype)) {
            case 1:    //SP2
            case 4:    //MP1
                $ping_type = 'ICMP';
                $retries = 1;
                break;
            default:
                $ping_type = 'UDP';
                $retries = 3;
        }

        if (!$this->host) {
            $this->ping_response = 'Destination address not specified';
            $this->ping_time = 'down';
            $this->ping_status = false;
            return false;
        }

        $to_sec = floor($timeout / 1000);
        $to_usec = ($timeout % 1000) * 1000;

        $this->ping_status = false;
        $this->ping_time = 'down';
        $this->ping_response = 'default';

        if ($ping_type === 'ICMP') {

            if (substr_count(strtolower(PHP_OS), 'sun')) {
                $result = shell_exec('ping ' . $this->host);
            } else if (substr_count(strtolower(PHP_OS), 'hpux')) {
                $result = shell_exec('ping -m ' . ceil($timeout / 1000) . ' -n ' . $retries . ' ' . $this->host);
            } else if (substr_count(strtolower(PHP_OS), 'mac')) {
                $result = shell_exec('ping -t ' . ceil($timeout / 1000) . ' -c ' . $retries . ' ' . $this->host);
            } else if (substr_count(strtolower(PHP_OS), 'freebsd')) {
                $result = shell_exec('ping -t ' . ceil($timeout / 1000) . ' -c ' . $retries . ' ' . $this->host);
            } else if (substr_count(strtolower(PHP_OS), 'darwin')) {
                $result = shell_exec('ping -t ' . ceil($timeout / 1000) . ' -c ' . $retries . ' ' . $this->host);
            } else if (substr_count(strtolower(PHP_OS), 'bsd')) {
                $result = shell_exec('ping -w ' . ceil($timeout / 1000) . ' -c ' . $retries . ' ' . $this->host);
            } else if (substr_count(strtolower(PHP_OS), 'aix')) {
                $result = shell_exec('ping -i ' . ceil($timeout / 1000) . ' -c ' . $retries . ' ' . $this->host);
            } else if (substr_count(strtolower(PHP_OS), 'winnt')) {
                $result = shell_exec('chcp 437 && ping -w ' . $timeout . ' -n ' . $retries . ' ' . $this->host);
            } else {
                $pattern = bin2hex($request);
                $result = shell_exec('ping -W ' . ceil($timeout / 1000) . ' -c ' . $retries . ' -p ' . $pattern . ' ' . $this->host . ' 2>&1');
                if (substr_count($result, 'unknown host') && file_exists('/bin/ping6')) {
                    $result = shell_exec('ping6 -W ' . ceil($timeout / 1000) . ' -c ' . $retries . ' -p ' . $pattern . ' ' . $this->host);
                }
            }

            if (strtolower(PHP_OS) != 'winnt') {
                $position = strpos($result, 'min/avg/max');

                if ($position > 0) {
                    $output = trim(str_replace(' ms', '', substr($result, $position)));
                    $pieces = explode('=', $output);
                    $results = explode('/', $pieces[1]);
                    $this->ping_status = true;
                    $this->ping_time = $results[1];
                    $this->ping_response = 'ICMP Ping Success (' . $this->ping_time . ' ms)';
                    return true;
                } else {
                    $this->ping_status = false;
                    $this->ping_time = 'down';
                    $this->ping_response = 'ICMP ping Timed out';
                    return false;
                }
            } else {
                $position = strpos($result, 'Minimum');

                if ($position > 0) {
                    $output = trim(substr($result, $position));
                    $pieces = explode(',', $output);
                    $results = explode('=', $pieces[2]);
                    $this->ping_status = true;
                    $this->ping_time = trim(str_replace('ms', '', $results[1]));
                    $this->ping_response = 'ICMP Ping Success (' . $this->ping_time . ' ms)';
                    return true;
                } else {
                    $this->ping_status = false;
                    $this->ping_time = 'down';
                    $this->ping_response = 'ICMP ping Timed out';
                    return false;
                }
            }

        } else

            if ($ping_type === 'UDP') {
                if (substr_count($this->host, ':') > 0) {
                    if (defined('AF_INET6')) {
                        $cs = socket_create(AF_INET6, SOCK_DGRAM, SOL_UDP);
                    } else {
                        $this->ping_response = 'PHP version does not support IPv6';
                        $this->ping_time = 'down';
                        $this->ping_status = false;
                        return false;
                    }
                } else {
                    $cs = socket_create(AF_INET, SOCK_DGRAM, SOL_UDP);
                }
                socket_set_nonblock($cs);
                socket_connect($cs, $this->host, $udp_port);
                $request = chr(0) . chr(1) . chr(0) . $request . chr(0);

                $error = '';
                $retry_count = 0;
                while (true) {
                    if ($retry_count >= $retries) {
                        $this->ping_status = false;
                        $this->ping_time = 'down';
                        $this->ping_response = 'UDP ping error: ' . $error;
                        @socket_shutdown($cs, 2);
                        socket_close($cs);
                        return false;
                    }

                    $timer_start_time = microtime(true);

                    socket_write($cs, $request, strlen($request));

                    $w = $f = array();
                    $r = array($cs);
                    $num_changed_sockets = socket_select($r, $w, $f, $to_sec, $to_usec);
                    if ($num_changed_sockets === false) {
                        $error = 'socket_select() failed, reason: ' . socket_strerror(socket_last_error());
                    } else {
                        switch ($num_changed_sockets) {
                            case 2: /* response received, so host is available */
                            case 1:
                                $start_time = $timer_start_time;
                                $end_time = microtime(true);
                                $time = number_format($end_time - $start_time, $precision);
                                @socket_recv($cs, $this->reply, 256, 0);
                                socket_last_error($cs);
                                $this->ping_status = true;
                                $this->ping_time = $time * 1000;
                                $this->ping_response = "UDP Ping Success (" . $this->ping_time . " ms)";
                                @socket_shutdown($cs, 2);
                                socket_close($cs);
                                return true;
                                break;
                            case 0: /* timeout */
                                $error = 'timeout';
                                break;
                        }
                    }
                    $retry_count++;
                }
            }
        return true;
    }
}
