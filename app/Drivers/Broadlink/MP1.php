<?php


namespace App\Drivers\Broadlink;


class MP1 extends Broadlink{

    function __construct($h = "", $m = "", $p = 80, $d = 0x4EB5) {

        parent::__construct($h, $m, $p, $d);

    }

    public function Set_Power_Mask($sid_mask, $state){

        $packet = self::bytearray(16);
        $packet[0x00] = 0x0d;
        $packet[0x02] = 0xa5;
        $packet[0x03] = 0xa5;
        $packet[0x04] = 0x5a;
        $packet[0x05] = 0x5a;
        $packet[0x06] = 0xb2 + ($state ? ($sid_mask<<1) : $sid_mask);
        $packet[0x07] = 0xc0;
        $packet[0x08] = 0x02;
        $packet[0x0a] = 0x03;
        $packet[0x0d] = $sid_mask;
        $packet[0x0e] = $state ? $sid_mask : 0;

        $this->send_packet(0x6a, $packet);
    }

    public function Set_Power($sid, $state){

        $sid_mask = 0x01 << ($sid - 1);

        $this->Set_Power_Mask($sid_mask, $state);
    }

    public function Check_Power_Raw(){

        $packet = self::bytearray(16);
        $packet[0x00] = 0x0a;
        $packet[0x02] = 0xa5;
        $packet[0x03] = 0xa5;
        $packet[0x04] = 0x5a;
        $packet[0x05] = 0x5a;
        $packet[0x06] = 0xae;
        $packet[0x07] = 0xc0;
        $packet[0x08] = 0x01;

        $response = $this->send_packet(0x6a, $packet);
        if (empty($response))
            return null;

        $err = hexdec(sprintf("%x%x", $response[0x23], $response[0x22]));

        if($err == 0){
            $enc_payload = array_slice($response, 0x38);

            if(count($enc_payload) > 0){

                $payload = $this->byte2array(self::aes128_cbc_decrypt($this->key(), $this->byte($enc_payload), $this->iv()));
                return $payload[0x0e];
            }

        }

        return null;


    }

    public function Check_Power(){

        $data = array();

        if(!is_null($state = $this->Check_Power_Raw())){
            if ($state & 0x01) $data[0] = 1; else $data[0] = 0;
            if ($state & 0x02) $data[1] = 1; else $data[1] = 0;
            if ($state & 0x04) $data[2] = 1; else $data[2] = 0;
            if ($state & 0x08) $data[3] = 1; else $data[3] = 0;
        }
        return $data;

    }

}
