<?php


namespace App\Drivers\Broadlink;


class A1 extends Broadlink
{

    function __construct($h = "", $m = "", $p = 80, $d = "0x2714")
    {

        parent::__construct($h, $m, $p, 0x2714);

    }

    public function Check_sensors()
    {

        $data = array();

        $packet = self::bytearray(16);
        $packet[0] = 0x01;

        $response = $this->send_packet(0x6a, $packet);
        if (empty($response))
            return false;

        $err = hexdec(sprintf("%x%x", $response[0x23], $response[0x22]));

        if ($err == 0) {
            $enc_payload = array_slice($response, 0x38);

            if (count($enc_payload) > 0) {

                $payload = $this->byte2array(self::aes128_cbc_decrypt($this->key(), $this->byte($enc_payload), $this->iv()));

                $data['temperature'] = ($payload[0x4] * 10 + $payload[0x5]) / 10.0;
                $data['humidity'] = ($payload[0x6] * 10 + $payload[0x7]) / 10.0;
                $data['light'] = $payload[0x8];
                $data['air_quality'] = $payload[0x0a];
                $data['noise'] = $payload[0x0c];

                switch ($data['light']) {
                    case 0:
                        $data['light_word'] = constant('LANG_BR_DARK');
                        break;
                    case 1:
                        $data['light_word'] = constant('LANG_BR_DIM');
                        break;
                    case 2:
                        $data['light_word'] = constant('LANG_BR_NORMAL');
                        break;
                    case 3:
                        $data['light_word'] = constant('LANG_BR_BRIGHT');
                        break;
                    default:
                        $data['light_word'] = constant('LANG_BR_UNKNOWN');
                        break;
                }

                switch ($data['air_quality']) {
                    case 0:
                        $data['air_quality_word'] = constant('LANG_BR_EXCELLENT');
                        break;
                    case 1:
                        $data['air_quality_word'] = constant('LANG_BR_GOOD');
                        break;
                    case 2:
                        $data['air_quality_word'] = constant('LANG_BR_NORMAL');
                        break;
                    case 3:
                        $data['air_quality_word'] = constant('LANG_BR_BAD');
                        break;
                    default:
                        $data['air_quality_word'] = constant('LANG_BR_UNKNOWN');
                        break;
                }

                switch ($data['noise']) {
                    case 0:
                        $data['noise_word'] = constant('LANG_BR_QUIET');
                        break;
                    case 1:
                        $data['noise_word'] = constant('LANG_BR_NORMAL');
                        break;
                    case 2:
                        $data['noise_word'] = constant('LANG_BR_NOISY');
                        break;
                    case 3:
                        $data['noise_word'] = constant('LANG_BR_EXTREME');
                        break;
                    default:
                        $data['noise_word'] = constant('LANG_BR_UNKNOWN');
                        break;
                }

            }

        }

        return $data;

    }

}
