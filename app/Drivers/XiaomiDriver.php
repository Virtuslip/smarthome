<?php

namespace App\Drivers;

use App\Jobs\XiaomiMotionDetected;
use App\Models\DataType;
use App\Models\Module;
use App\Models\SObject;
use App\Models\SObjectData;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Log;

class XiaomiDriver
{
    private static $dataTypes;
    private static $paramTypes;

    private function makeSignature($token, $key)
    {
        $iv = hex2bin('17996d093d28ddb3ba695a2e6f58562e');
        $bin_data = base64_decode(openssl_encrypt($token, 'AES-128-CBC', $key, OPENSSL_ZERO_PADDING, $iv));
        $res = bin2hex($bin_data);
        return $res;
    }

    public function SendCommand(SObject $device, SObjectData $prop, $value)
    {
        $error = false;
        $model = $device->data->firstWhere('code', 'model');
        switch ($prop->code) {
            case 'channel_0':
            case 'channel_1':
                if ($value == '1') {
                    $value = 'on';
                } else {
                    $value = 'off';
                }
                break;
        }
        $gatewayID = $device->data->firstWhere('code', 'gateway');
        $gateway = SObject::where('id', $gatewayID->value)->with('data')->first();
        if (empty($gateway)) {
            $error = true;
            Log::error('Шлюз не найден');
        }

        if (!$error) {
            $token = $gateway->data->firstWhere('code', 'token');
            if (empty($token)) {
                $error = true;
                Log::error('Токен не найден');
            }
            $apiKey = $gateway->data->firstWhere('code', 'api_key');
            if (empty($apiKey) || empty($apiKey->value)) {
                $error = true;
                Log::error('Ключ не найден');
            }

            $gatewayIP = $gateway->data->firstWhere('code', 'ip');
            if (empty($gatewayIP) || empty($gatewayIP->value)) {
                $error = true;
                Log::error('IP не найден');
            }
        }

        if (!$error) {
            $signature = $this->makeSignature($token->value, $apiKey->value);
            $data = array('key' => $signature);
            $data[] = [$prop->code => $value];
            $command = array(
                'cmd' => 'write',
                'model' => $model->value,
                'sid' => $device->uid,
                'data' => $data
            );

            if (!($sock = socket_create(AF_INET, SOCK_DGRAM, SOL_UDP))) {
                $errorcode = socket_last_error();
                $errormsg = socket_strerror($errorcode);
                Log::error($errormsg);
            }
            socket_set_option($sock, SOL_SOCKET, SO_BROADCAST, SO_REUSEADDR);
            if (!socket_bind($sock, '0.0.0.0', 9899)) {
                $errorcode = socket_last_error();
                $errormsg = socket_strerror($errorcode);
                Log::error($errormsg);
            }

            $msg = json_encode($command);
            socket_sendto($sock, $msg, strlen($msg), 0, $gatewayIP->value, 9898);
            @$r = socket_recvfrom($sock, $buf, 1024, 0, $remote_ip, $remote_port);
            socket_close($sock);
        }


    }

    public function Sync()
    {
        $sock = false;

        if (!($sock = socket_create(AF_INET, SOCK_DGRAM, SOL_UDP))) {
            $errormsg = socket_strerror(socket_last_error());
            Log::error($errormsg);
        }

        socket_set_option($sock, SOL_SOCKET, SO_BROADCAST, 1);
        socket_set_option($sock, IPPROTO_IP, IP_MULTICAST_LOOP, true);
        socket_set_option($sock, IPPROTO_IP, IP_MULTICAST_TTL, 32);
        socket_set_option($sock, IPPROTO_IP, MCAST_JOIN_GROUP, array('group' => '224.0.0.50', 'interface' => 0, 'source' => 0));
        socket_set_option($sock, SOL_SOCKET, SO_BROADCAST, SO_REUSEADDR);

        if (!socket_bind($sock, '0.0.0.0', 9898)) {
            $errormsg = socket_strerror(socket_last_error());
            Log::error($errormsg);
        }

        $module = Module::where('code', 'xiaomi')->first();

        if (!empty($module) && $module->active) {
            $arTypeData = DataType::all();
            foreach ($arTypeData as $type) {
                self::$dataTypes[$type['code']] = $type->id;
            }

            while (true) {
                @$r = socket_recvfrom($sock, $buf, 1024, 0, $ip, $port);
                if (!empty($buf)) {
//                    Log::info($buf);
                    $deviceInfo = json_decode($buf, true);
                    $smartObject = SObject::where('uid', $deviceInfo['sid'])->with('data')->first();
                    if (empty($smartObject)) {
                        $smartObject = new SObject();
                        $smartObject->module_id = $module->id;
                        $smartObject->uid = $deviceInfo['sid'];
                        $smartObject->active = true;
                        $smartObject->name = self::getObjectName($deviceInfo['model']);
                        $smartObject->code = 'xiaomi_device_' . time();
                        $smartObject->save();
                    }

                    if (empty($smartObject->name)) {
                        $smartObject->name = self::getObjectName($deviceInfo['model']);
                    }

                    //Модель устройства
                    if (empty($smartObject->data->firstWhere('code', 'model'))) {
                        $model = new SObjectData();
                        $model->code = 'model';
                        $model->sobject_id = $smartObject->id;
                        $model->name = 'Модель';
                        $model->type_id = self::$dataTypes['string'];
                        $model->value = $deviceInfo['model'];
                        $model->is_readonly = true;
                        $model->is_system = true;
                        $smartObject->data()->save($model);
                    }

                    if ($deviceInfo['model'] == 'gateway') {
                        //Ключ api
                        if (empty($smartObject->data->firstWhere('code', 'api_key'))) {
                            $model = new SObjectData();
                            $model->code = 'api_key';
                            $model->sobject_id = $smartObject->id;
                            $model->name = 'Ключ';
                            $model->type_id = self::$dataTypes['string'];
                            $model->value = '';
                            $model->is_readonly = false;
                            $model->is_system = true;
                            $smartObject->data()->save($model);
                        }

                        if (!empty($deviceInfo['token'])) {
                            $token = $smartObject->data->firstWhere('code', 'token');
                            if (empty($token)) {
                                $token = new SObjectData();
                                $token->code = 'token';
                                $token->sobject_id = $smartObject->id;
                                $token->name = 'Токен';
                                $token->type_id = self::$dataTypes['string'];
                                $token->is_readonly = true;
                                $token->is_system = true;
                            }
                            $token->value = $deviceInfo['token'];
                            $smartObject->data()->save($token);
                        }
                    } else {
                        //Выставляем шлюз
                        if (empty($smartObject->data->firstWhere('code', 'gateway'))) {
                            $gateway = SObject::whereHas('data', function (Builder $query) {
                                $query->where('code', 'model');
                                $query->where('value', 'gateway');
                            })->first();
                            if (!empty($gateway)) {
                                $model = new SObjectData();
                                $model->value = $gateway->id;
                                $model->code = 'gateway';
                                $model->sobject_id = $smartObject->id;
                                $model->name = 'Шлюз';
                                $model->is_readonly = true;
                                $model->is_system = true;
                                $model->type_id = self::$dataTypes['object'];
                                $smartObject->data()->save($model);
                            }
                        }
                    }

                    if (empty($smartObject->name)) {
                        $smartObject->name = self::getObjectName($deviceInfo['model']);
                    }
                    $smartObject->save();

                    $deviceData = json_decode($deviceInfo['data'], true);
                    foreach ($deviceData as $code => $value) {
                        $data = $smartObject->data->firstWhere('code', $code);
                        if (empty($data)) {
                            $data = new SObjectData();
                            $data->code = $code;
                            $data->sobject_id = $smartObject->id;
                            $data->type_id = self::$dataTypes['string'];
                            $data->is_readonly = true;
                            $data->is_system = true;
                            $data->name = self::getObjectDataName($code, $deviceInfo['model']);
                            switch ($code) {
                                case 'channel_0':
                                case 'channel_1':
                                    $data->type_id = self::$dataTypes['switcher'];
                                    break;
                            }
                        }
                        if (empty($data->name)) {
                            $data->name = self::getObjectDataName($code, $deviceInfo['model']);
                        }

                        switch ($code) {
                            case 'channel_0':
                            case 'channel_1':
                                if ($value == 'on') {
                                    $value = '1';
                                } else {
                                    $value = '0';
                                }
                                break;
                            case 'status':
                                if($deviceInfo['model'] == 'plug'){
                                    if ($value == 'on') {
                                        $value = '1';
                                    } else {
                                        $value = '0';
                                    }
                                }
                                break;
                            case 'voltage':
                                $value = round($value / 1000, 2);
                                break;
                            case 'temperature':
                                $value = round($value / 100, 1);
                                break;
                            case 'humidity':
                                $value = round($value / 100, 1);
                                break;
                            case 'pressure':
                                $value = round($value / 100, 0);
                                break;
                        }
                        if ($value == 'motion' && $data->id > 0) {
                            $job = (new XiaomiMotionDetected($data->id))->delay(Carbon::now()->addSeconds(10));
                            dispatch($job);
                        }
                        $data->value = $value;
                        $smartObject->data()->save($data);
                    }
                }
            }
        }


    }


    static function getObjectName($model)
    {
        $name = '';
        switch ($model) {
            case 'sensor_motion.aq2':
                $name = 'Aqara Motion Sensor';
                break;
            case 'magnet':
                $name = 'Датчик открытия Aqara';
                break;
            case 'plug':
                $name = 'Мини розетка Aqara';
                break;
            case 'ctrl_neutral2':
                $name = 'Xiaomi Wired Dual Wall Switch';
                break;
            case 'ctrl_neutral1':
                $name = 'Xiaomi Wired Single Wall Switch';
                break;
            case 'sensor_wleak.aq1':
                $name = 'Aqara Water Leak Sensor';
                break;
            case 'gateway':
                $name = 'Aqara Gateway';
                break;
            case 'weather.v1':
                $name = 'Aqara temperature humidity sensor';
                break;
        }
        return $name;
    }

    static function getObjectDataName($code, $model)
    {
        $name = '';
        switch ($code) {
            case 'rgb':
                $name = 'RGB';
                break;
            case 'illumination':
                $name = 'Яркость';
                break;
            case 'ip':
                $name = 'IP';
                break;
            case 'channel_0':
                if ($model == 'ctrl_neutral2') {
                    $name = 'Левая клавиша';
                } else {
                    $name = 'Статус';
                }
                break;
            case 'channel_1':
                $name = 'Правая клавиша';
                break;
            case 'voltage':
                $name = 'Напряжение';
                break;
            case 'no_motion':
                $name = 'Нет движения (сек)';
                break;
            case 'temperature':
                $name = 'Температура';
                break;
            case 'humidity':
                $name = 'Влажность';
                break;
            case 'pressure':
                $name = 'Давление';
                break;
            case 'lux':
                $name = 'Освещенность';
                break;
            case 'status':
                $name = 'Статус';
                break;

        }
        return $name;
    }
}
