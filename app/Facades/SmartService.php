<?php


namespace App\Facades;
use Illuminate\Support\Facades\Facade;

class SmartService extends Facade
{
    protected static function getFacadeAccessor(){
        return 'Smart';
    }
}
