<?php

namespace App\Services;

use App\Drivers\BroadlinkDriver;
use App\Drivers\smsru\SMSRU;
use App\Drivers\XiaomiDriver;
use App\Models\Module;
use App\Models\SObject;
use App\Models\SObjectData;
use Illuminate\Support\Facades\Log;

class Smart
{
    public function GetProperty($deviceCode, $propertyCode)
    {
        $result = false;
        $smartObject = SObject::where('code', $deviceCode)->first();
        if (empty($smartObject)) {
            Log::error('Объект с кодом ' . $deviceCode . ' не найден');
        } else {
            $smartProp = SObjectData::where('code', $propertyCode)->where('sobject_id', $smartObject->id)->first();
            if (empty($smartProp)) {
                Log::error('Свойство с кодом ' . $propertyCode . ' не найдено');
            } else {
                $result = $smartProp->value;
            }
        }
        return $result;
    }

    public function SetProperty($deviceCode, $propertyCode, $value)
    {
        $smartObject = SObject::where('code', $deviceCode)->with('data')->first();

        if (empty($smartObject)) {
            Log::error('Объект с кодом ' . $deviceCode . ' не найден');
        } else {
            $smartProp = SObjectData::where('code', $propertyCode)->where('sobject_id', $smartObject->id)->first();

            if (empty($smartProp)) {
                Log::error('Свойство с кодом ' . $propertyCode . ' не найдено');
            } else {
                switch ($smartObject->module->code) {
                    case 'xiaomi':
                        $driver = new XiaomiDriver();
                        $driver->SendCommand($smartObject, $smartProp, $value);
                        break;
                    case 'broadlink':
                        $driver = new BroadlinkDriver();
                        $driver->SendCommand($smartObject, $smartProp, $value);
                        break;
                }
            }
        }
    }

    public function sendSms($text){
        $module = Module::where('code', 'smsru')->with('data')->first();
        if(!empty($module) && $module->active){
            $apiKey = $module->data->firstWhere('code', 'api_id');
            if(empty($apiKey)){
                Log::error($module->name.' Не задан API ID');
                return false;
            }
            $phone = $module->data->firstWhere('code', 'to');
            if(empty($phone)){
                Log::error($phone->name.' Не задан номер телефона');
                return false;
            }
            $smsru = new SMSRU($apiKey->value);
            $data = new \stdClass();
            $data->to = $phone->value;
            $data->text = $text;
            $sms = $smsru->send_one($data);
            if ($sms->status == "OK") { // Запрос выполнен успешно
                echo "Сообщение отправлено успешно. ";
                echo "ID сообщения: $sms->sms_id. ";
                echo "Ваш новый баланс: $sms->balance";
            } else {
                echo "Сообщение не отправлено. ";
                echo "Код ошибки: $sms->status_code. ";
                echo "Текст ошибки: $sms->status_text.";
            }
        }

    }
}
