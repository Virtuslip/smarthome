<?php

namespace App\Http\Controllers;

use App\Models\Dashboard;
use App\Models\DashboardDisplayType;
use App\Models\DashboardItem;
use App\Models\SObject;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rule;

class DashboardItemController extends Controller
{
    public function init()
    {
        $result['display_type'] = DashboardDisplayType::all();
        $result['objects'] = SObject::with('data')->get();
        return response()->json($result);
    }

    public function getList(Request $request)
    {
        $result = DashboardItem::with('dashboard', 'object_item', 'type')->where('dashboard_id', $request->dashboard)->get();
        return response()->json($result);
    }

    public function edit(Request $request)
    {
        $result = DashboardItem::with('dashboard', 'object_item')->find($request->id);
        $result->settings = json_decode($result->settings, true);
        return response()->json($result);
    }

    public function remove(Request $request)
    {
        $item = DashboardItem::find($request->id);
        $item->delete();
        return response()->json(true);
    }

    public function save(Request $request)
    {
        if ($request->id == null) {
            $item = new DashboardItem();
        } else {
            $item = DashboardItem::find($request->id);
        }
        $item->dashboard_id = $request->dashboard_id;
        $item->object_item_id = $request->object_item_id;
        $item->display_type = $request->display_type;
        $item->display_type = $request->display_type;
        $item->name = $request->name;
        $item->settings = json_encode($request->settings);
        $item->save();
        return response()->json($request);
    }
}
