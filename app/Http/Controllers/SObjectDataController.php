<?php

namespace App\Http\Controllers;

use App\Models\DataType;
use App\Models\Script;
use App\Models\SObjectData;
use Illuminate\Http\Request;
use Smart;

class SObjectDataController extends Controller
{
    public function sendCommand(Request $request){
        Smart::SetProperty($request->object, $request->prop, $request->value);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $items = SObjectData::where('sobject_id', $id)->with('type')->get();
        return response()->json($items);
    }

    function edit($object, $id)
    {
        $result = [];
        $result['types'] = DataType::all();
        $result['scripts'] = Script::all();
        if ($id > 0) {
            $result['item'] = $this->GetItem($id);
        } else {
            $result['item']  = [
                'name' => '',
                'active' => 1,
                'sobject_id' => $object
            ];
        }
        return response()->json($result);
    }

    public function save(Request $request)
    {
        if ($request->id > 0) {
            $item = SObjectData::find($request->id);
        } else {
            $item = new SObjectData();
            $item->sobject_id = $request->sobject_id;
        }
        $item->code = $request->code;
        $item->name = $request->name;
        $item->value = $request->value;
        $item->type_id = $request->type_id;
        $item->script_id = $request->script_id;
        $item->active = $request->active;
        $item->save();
        return response()->json($item);
    }

    function remove($id)
    {
        $item = SObjectData::find($id);
        $item->delete();
        return response()->json(true);
    }

    function GetItem($id)
    {
        return SObjectData::where('id', $id)->first();
    }

}
