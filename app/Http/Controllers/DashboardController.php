<?php

namespace App\Http\Controllers;

use App\Models\Dashboard;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rule;

class DashboardController extends Controller
{


    public function index()
    {
        $items = Dashboard::with('items', 'items.object_item', 'items.object_item.sobject', 'items.type')->orderBy('order')->get();
        foreach ($items as $item){
            foreach ($item->items as &$prop){
                if(!empty($prop->settings)){
                    $prop->settings = json_decode($prop->settings, true);
                }
            }
        }
        return response()->json($items);
    }

    public function edit(Request $request)
    {
        $result = Dashboard::find($request->id);
        return response()->json($result);
    }

    public function remove(Request $request)
    {
        $item = Dashboard::find($request->id);
        $item->delete();
        return response()->json(true);
    }

    public function save($id, Request $request)
    {
        if ($id > 0) {
            $item = Dashboard::findOrFail($id);
        } else {
            $item = new Dashboard();
        }
        $validatedData = $request->validate([
            'name' => 'required',
        ]);

        $item->name = $request->name;
        $item->order = $request->order;
        $item->save();
        return response()->json($item);
    }
}
