<?php

namespace App\Http\Controllers;
use App\Models\Module;
use App\Models\ModuleSettings;
use Illuminate\Http\Request;

class ModuleController extends Controller
{
    public function index()
    {
        $items = Module::all();
        return response()->json($items);
    }

    //Изменение активности
    public function changeActivity(Request $request){
        $item = Module::where ($request->id);
    }

    public function edit($id)
    {
        $item = Module::where('id', $id)->with('data', 'data.type')->first();
        return response()->json($item);
    }

    public function save(Request $request)
    {
        $item = Module::find($request->id);
        foreach ($request->data as $data){
            $dataItem = ModuleSettings::findOrFail($data['id']);
            $dataItem->value = $data['value'];
            $dataItem->save();
        }
        $item->save();
        $item = Module::where('id', $request->id)->with('data', 'data.type')->first();
        return response()->json($item);
    }
}
