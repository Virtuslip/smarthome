<?php

namespace App\Http\Controllers;

use App\Models\Module;
use App\Models\SObject;
use App\Models\SObjectData;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class SObjectController extends Controller
{
    public function index()
    {
        $items = SObject::with('module')->get();
        return response()->json($items);
    }

    public function edit($id)
    {
        $item = $this->GetItem($id);
        return response()->json($item);
    }

    public function save(Request $request)
    {
        if ($request['id'] > 0) {
            $item = SObject::find($request->id);
        } else {
            $module = Module::where('code', 'base')->first();
            $item = new SObject();
            $item->module_id = $module->id;
        }
        $validatedData = $request->validate([
            'code' => ['required', Rule::unique('sobjects')->ignore($item->id)],
            'name' => 'required',
        ]);
        $item->code = $request->code;
        $item->name = $request->name;
        $item->active = $request->active;
        $item->save();
        $item = $this->GetItem($item->id);
        return response()->json($item);
    }

    function remove($id)
    {
        $item = SObject::find($id);
        $item->delete();
        return response()->json(true);
    }

    function GetItem($id)
    {
        return SObject::where('id', $id)->first();
    }
}
