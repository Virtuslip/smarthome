<?php

namespace App\Http\Controllers;

use App\Jobs\SheduleRunScript;
use App\Models\Module;
use App\Models\Script;
use App\Models\Directory\ScriptWhenRun;
use App\Models\SObject;
use App\Models\SObjectData;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rule;

class ScriptsController extends Controller
{
    public function index()
    {

        $items = Script::all();
        return response()->json($items);
    }

    public function edit($id)
    {
        $result = [];
        $result['when_run'] = ScriptWhenRun::all();
        if($id> 0){
            $result['item'] = Script::findOrFail($id);
        }

        return response()->json($result);
    }

    public function save(Request $request)
    {
        if ($request['id'] > 0) {
            $item = Script::find($request->id);
        } else {
            $item = new Script();
        }
        $validatedData = $request->validate([
            'code' => ['required', Rule::unique('scripts')->ignore($item->id)],
            'name' => 'required',
        ]);
        $item->code = $request->code;
        $item->name = $request->name;
        $item->active = $request->active;
        $item->when_run = $request->when_run;
        $item->shedule_time = $request->shedule_time;
        $item->action = $request->action;
        $item->save();
        $item = Script::findOrFail($item->id);
        return response()->json($item);
    }

    function remove($id)
    {
        $item = Script::find($id);
        $item->delete();
        return response()->json(true);
    }
}
